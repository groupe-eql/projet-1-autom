package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PageEditCriteria extends PageCreateCriteria{
    @FindBy(xpath = "//td[contains(text(),'Modifier Type de critère')]")
    WebElement editTitle;

    //assert that the elements needed are displayed
    public void checkOnPage(String name,WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(editTitle));
        assertEquals("Title is wrong","Modifier Type de critère: "+name,editTitle.getText());
        assertEquals("Form title doesn't match","Modifier",formTitle.getText());
        assertTrue("Save button is missing",saveButton.isDisplayed());
        assertTrue("Save and continue button is missing",saveContinueButton.isDisplayed());
        assertTrue("Cancel button is missing",cancelButton.isDisplayed());
    }
    //asset that teh confirmation message is displayed after save
    public void checkSaved(String name,WebDriver driver){
        WebElement saveBox=driver.findElement(By.xpath("//span[contains(text(),'Type de critère \""+name+"\" enregistré')]"));
        assertTrue(saveBox.isDisplayed());
    }


}
