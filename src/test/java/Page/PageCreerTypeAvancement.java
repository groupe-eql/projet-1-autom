package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageCreerTypeAvancement {
    @FindBy(xpath = "//td[contains(@id,'4-cnt')]")
    private WebElement titleCreerTypeAvancement;
    @FindBy(xpath = "//div[contains(@id,'4-hm')]")
    private WebElement tableauTile;
    @FindBy(xpath = "//div[contains(@id,'45')]")
    private WebElement nomUnite;
    //input[contains(@id,'U55')]
    @FindBy(xpath = "//input[contains(@id,'55')]")
    private WebElement champNomUnite;
    @FindBy(xpath = "//div[contains(@id,'75')]")
    private WebElement actif;
    @FindBy(xpath = "//input[contains(@id,'85')]")
    private WebElement champActif;
    @FindBy(xpath = "//div[contains(@id,'a5')]")
    private WebElement valeurMax;
    @FindBy(xpath = "//input[contains(@id,'b5')]")
    private WebElement champValeurMax;
    @FindBy(xpath = "//div[contains(@id,'d5')]")
    private WebElement precision;
    @FindBy(xpath = "//input[contains(@id,'e5')]")
    private WebElement champPrecision;
    @FindBy(xpath = "//div[contains(@id,'g5')]")
    private WebElement type;
    @FindBy(xpath = "//span[contains(@id,'h5')]")
    private WebElement champType;
    @FindBy(xpath = "//div[contains(@id,'j5')]")
    private WebElement pourcentage;
    @FindBy(xpath = "//span[contains(@id,'k5')]")
    private WebElement champPourcentage;
    @FindBy (xpath = "//td[contains(text(),'Enregistre')]")
    private WebElement btnEnregistre;
    @FindBy (xpath = "//td[contains(text(),'Annuler')]")
    private WebElement btnAnnuler;
    @FindBy (xpath = "//td[contains(text(),'Sauver')]")
    private WebElement btnSauverContinuer;
    public boolean checkTableauTitle(){
        return tableauTile.isDisplayed();
    }
    public boolean checktitleCreerTypeAvancement() throws InterruptedException {
        Thread.sleep(2000);
        return  titleCreerTypeAvancement.isDisplayed();
    }
    public boolean checkLigneTableau(){
        return nomUnite.isDisplayed() && actif.isDisplayed() && valeurMax.isDisplayed()
                && precision.isDisplayed() && type.isDisplayed() && pourcentage.isDisplayed();
    }
    public String champNomUnite(){
        return champNomUnite.getText();
    }
    public boolean champActif(){
        return champActif.isSelected();
    }
    public String champValeurMax(){
        return champValeurMax.getText();
    }
    public String champPrecision(){
        return champPrecision.getText();
    }
    public String champType(){
        return champType.getText();
    }
    public boolean champPourcentage(){
        return champPourcentage.isSelected();
    }
    public boolean checkBtnEnregistrer(){
        return btnEnregistre.isDisplayed();
    }
    public boolean checkBtnSauverContinuer(){
        return btnSauverContinuer.isDisplayed();
    }
    public boolean checkBtnAnnuler(){
        return btnAnnuler.isDisplayed();
    }
    public PageTypeAvancement SaisirTableau(WebDriver driver, String nom, String valeur){
        champNomUnite.sendKeys(nom);
        champActif.click();
        champValeurMax.clear();
        champValeurMax.sendKeys(valeur);
        btnEnregistre.click();
        return PageFactory.initElements(driver, PageTypeAvancement.class);
    }
    public void saisirChamps(String nom,WebDriver driver){
        champNomUnite.sendKeys(nom);
        champPourcentage.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.attributeToBe(champValeurMax,"disabled",""));
    }
    public void SauverContinuerTest2(){
        btnSauverContinuer.click();
    }
    @FindBy(xpath = "//span[contains(text(),'Type ')]")
    private WebElement message;
    @FindBy(xpath = "//td[contains(text(),'Modifier Type')]")
    private WebElement titleDisplayed;

    public boolean messageApparu(){
      return   message.isDisplayed();
    }
    public String titleDisplayed(){
      return  titleDisplayed.getText();
    }
    public PageTypeAvancement clickBtnAnnuler(WebDriver driver){
        btnAnnuler.click();
    return PageFactory.initElements(driver, PageTypeAvancement.class);}




}
