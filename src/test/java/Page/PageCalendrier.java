// |||||||||||||||||||||||||||||||||||||||||
// || Page : DEBUT>Ressources>Calendriers ||
// |||||||||||||||||||||||||||||||||||||||||

package Page;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class PageCalendrier {

    private WebDriver driver;
    // Déclaration des éléments de la page avec leurs XPath
    @FindBy(xpath = "//button[@type='button' and text()='Ressources ']")
    private WebElement list_ressource; // Bouton 'Ressources'
    @FindBy(xpath = "//a[@class='z-menu-item-cnt' and @href='/libreplan/calendars/calendars.zul']")
    private WebElement item_calendar; // Lien vers la page des calendriers
    @FindBy(xpath = "//th[@class='z-treecol']//div[text()='Nom']")
    private WebElement tableCalendar_nom; // Colonne 'Nom' dans le tableau des calendriers
    @FindBy(xpath = "//th[@class='z-treecol']//div[text()='Hérité de la date']")
    private WebElement tableCalendar_heritedeladate; // Colonne 'Hérité de la date'
    @FindBy(xpath = "//th[@class='z-treecol']//div[text()='Héritages à jour']")
    private WebElement tableCalendar_heritageajour; // Colonne 'Héritages à jour'
    @FindBy(xpath = "//th[@class='z-treecol']//div[text()='Opérations']")
    private WebElement tableCalendar_operations; // Colonne 'Opérations'
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Créer']")
    public WebElement tableCalendar_button_creer; // Bouton 'Créer' pour ajouter un calendrier
    @FindBy(xpath = "//td[@class='z-caption-l' and text()='Créer Calendrier']")
    private WebElement titreCreerCalendrier; // Titre de la page 'Créer Calendrier'
    @FindBy(xpath = "//span[@class='z-tab-text' and text()='Données de calendrier']")
    private WebElement DonneeCalendrier; // Onglet 'Données de calendrier'
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Enregistrer']")
    private WebElement EnregistrerButton; // Bouton 'Enregistrer'
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Enregistrer et continuer']")
    private WebElement EnregistrerEtContinuerButton; // Bouton 'Enregistrer et continuer'
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Annuler']")
    private WebElement annulerButton; // Bouton 'Annuler'
    @FindBy(xpath = "//input[substring(@id, string-length(@id) - 1) = '45']")
    private WebElement Nom_field; // Champ de saisie pour le nom du calendrier
    @FindBy(xpath = "//input[contains(@id, 'd5-real')]")
    private WebElement genererlecode_checkbox; // Case à cocher pour générer le code
    @FindBy(xpath = "//tbody[2]/tr[2]/td[4]/div/span[1]/table/tbody/tr[2]/td[2]/img")
    private WebElement Cree_Derive; // Bouton pour créer un calendrier dérivé
    @FindBy(xpath = "//span[3]/table[1]/tbody[1]/tr[2]/td[2]/img[1]")
    private WebElement modif_cal; // Bouton pour modifier en calendrier
    @FindBy(xpath = "//td[@class='z-button-cm' and text()='Créer une exception']")
    private WebElement cree_exception; // Bouton pour créer une exception
    @FindBy(xpath = "//td[@class=\"z-button-cm\" and text()=\"Mettre à jour l'exception\"]")
    private WebElement mettre_a_jour_exception_bouton; // Bouton pour mettre à jour l'exception
    @FindBy(xpath = "//div[@class='z-errbox-center']")
    private WebElement msg_erreur_exception; // Message d'erreur d'exception
    @FindBy(xpath = "//table[1]/tbody[1]/tr[1]/td[3]/i[1]//input[@class='z-datebox-inp']")
    private WebElement date_debut_field; // champs date de début periode d'exception
    @FindBy(xpath = "//table[1]/tbody[1]/tr[1]/td[7]/i[1]//input[@class='z-datebox-inp']")
    private WebElement date_fin_field; // champs date de fin periode d'exception
    @FindBy(xpath = "//span[@class='z-label' and text()='STRIKE']")
    private WebElement ligne_exception; // ligne de l'exception STRIKE
    @FindBy(xpath = "//td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[3]/td[1]//span[@class='z-label' and text()='0:0']")
    private WebElement temps_travaille_exception; // champs de temps travaillé
    @FindBy(xpath = "//span[@class='z-label' and text()='Exception: STRIKE']")
    private WebElement type_exceptionstrike; // champs de type d'exeption strike
    @FindBy(xpath = "//div[3]/div[1]/div[3]/table[1]/tbody[2]/tr[2]/td[4]/div[1]//span[3]//img[1]")
    private WebElement modif_cal_test1;
    @FindBy(xpath = "//td[3]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/i[1]//input[@class='z-spinner-inp']")
    private WebElement heure_effort_normal;
    @FindBy(xpath = "//td[3]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[3]/i[1]//input[@class='z-spinner-inp']")
    private WebElement minute_effort_normal;
    @FindBy(xpath = "//td[7]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/i[1]//input[@class='z-spinner-inp']")
    private WebElement heure_effort_supp;
    @FindBy(xpath = "//td[7]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[3]/i[1]//input[@class='z-spinner-inp']")
    private WebElement minute_effort_supp;
    @FindBy(xpath = "//div/span[@class='z-label' and text()='Hérité']")
    private WebElement origine_herite;

    @FindBy(xpath = "//div/span[@class='z-label' and text()='Direct']")
    private WebElement origine_direct;
    @FindBy(xpath = "//span[@class='z-label' and text()='STRIKE']")
    private WebElement typeexception_STRIKE;
    @FindBy(xpath = "//tbody[2]/tr[2]/td[4]/div/span[2]/table/tbody/tr[2]/td[2]/img")
    private WebElement Cree_copieCal; // Bouton pour créer une copie de calendrier
    @FindBy(xpath = "//tbody[2]/tr[1]/td[4]/div/span[2]/table/tbody/tr[2]/td[2]/img")
    private WebElement Cree_copieCal2; // Bouton pour créer une copie 2 de calendrier
    @FindBy(xpath = "//span[@class='z-label' and text()='Dérivé du calendrier test1']")
    private WebElement type_calendrier; // Message indiquant le type de calendrier
    @FindBy(xpath = "//span[@class='z-label' and text()='test1 existe déjà']")
    private WebElement msg_existe_deja; // Message d'erreur si le calendrier existe déjà
    @FindBy(xpath = "//span[@class='z-label' and text()='test1 existe déjà']")
    private WebElement msg_copie_existe_deja; // Message d'erreur pour la copie existante
    @FindBy(xpath = "//span[@class='z-label' and text()='Calendrier de base \"Calendrier - Test Calendrier Dérivé\" enregistré']")
    private WebElement msg_enregistre; // Message de confirmation d'enregistrement de dérivé
    @FindBy(xpath = "//span[@class='z-dottree-ico z-dottree-root-open']")
    private WebElement reduire_derive; // Bouton pour réduire l'arbre du calendrier dérivé
    @FindBy(xpath = "//td[@class='z-caption-l' and text()='Créer Calendrier: test1']")
    private WebElement titre_copieCal; // Titre lors de la création d'une copie de calendrier
    @FindBy(xpath = "//span[@class='z-label' and text()='Calendrier source']")
    private WebElement type_cal; // Champs type de calendrier
    @FindBy(xpath = "//span[@class='z-label' and text()='Calendrier de base \"Calendrier - Test 2\" enregistré']")
    private WebElement msgcaltest2enregistre; // Message de confirmation pour calendrier test2 enregistré

    public String id_Xpath(WebDriver driver,String id){
       WebElement div= driver.findElement(By.xpath("//body/div"));
       String statique_part = id.substring(4);
       String dynamique_part_id = div.getAttribute("id").substring(0, 4);
        //System.out.println(dynamique_part_id+statique_part);
        return dynamique_part_id+statique_part;
    }

    // Méthode pour vérifier la présence des éléments sur la page Liste de Calendriers
    public void CheckOnPageListeCalendriers() {
        // Vérifications pour s'assurer que tous les éléments nécessaires sont présents
        assertTrue("'Nom' est manquant", tableCalendar_nom.isDisplayed());
        assertTrue("'Hérité de la date' est manquant", tableCalendar_heritedeladate.isDisplayed());
        assertTrue("'Hérité à jour' est manquant", tableCalendar_heritageajour.isDisplayed());
        assertTrue("'Opérations' est manquant", tableCalendar_operations.isDisplayed());
        assertTrue("'Button Créer' est manquant", tableCalendar_button_creer.isDisplayed());
    }

    // Méthode pour vérifier la présence des éléments sur la page Créer Calendrier
    public void CheckOnPageCreerCalendrier() {
        // Vérifications pour s'assurer que tous les éléments nécessaires sont présents sur la page de création
        assertTrue("'Titre' est manquant", titreCreerCalendrier.isDisplayed());
        assertTrue("'Bouton Enregistrer' est manquant", EnregistrerButton.isDisplayed());
        assertTrue("'Formulaire : Données de calendrier' est manquant", DonneeCalendrier.isDisplayed());
        assertTrue("'Bouton Enregistrer et continuer' est manquant", EnregistrerEtContinuerButton.isDisplayed());
        assertTrue("'Bouton Annuler' est manquant", annulerButton.isDisplayed());
    }

    // Méthode pour ajouter un calendrier avec un nom donné
    public void AddCalendar(WebDriver driver) {
        // Cliquer sur le bouton "Créer"
        tableCalendar_button_creer.click();
        // Efface et entre un nouveau nom dans le champ du calendrier, puis sauvegarde

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement Nom_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[substring(@id, string-length(@id) - 1) = '45']")));

        Nom_field.clear();
        Nom_field.sendKeys("test1");
        // Coche la case si elle n'est pas déjà cochée
        if (!genererlecode_checkbox.isSelected()) {
            genererlecode_checkbox.click();
        }
        EnregistrerButton.click();
        // Vérifie que le calendrier est bien créé
        WebElement calendrier_cree = driver.findElement(By.xpath("//span[@class='z-label' and text()='test1']"));
        assertTrue("Calendrier Test1 n'est pas créé", calendrier_cree.isDisplayed());

    }


    // Méthode pour ajouter un calendrier dérivé
    public void AddDeriveCal(WebDriver driver) {

        Cree_Derive.click(); // Clique sur le bouton pour créer un calendrier dérivé
        CheckOnPageCreerCalendrier(); // Vérifie la présence des éléments sur la page de création
        String competent_nom = Nom_field.getAttribute("value");
        Assert.assertTrue("le champ nom n'est pas vide", competent_nom.isEmpty()); // Vérifie que le champ nom est vide
        assertTrue("'Type de Calendrier' est manquant", type_calendrier.isDisplayed()); // Vérifie la présence du type de calendrier
        Nom_field.sendKeys("test1"); // Saisit le nom du calendrier : test1
        EnregistrerButton.click(); // Clique sur le bouton enregistrer
        assertTrue("message de warning: 'test1 existe déjà' est manquant", msg_existe_deja.isDisplayed()); // Vérifie si le calendrier test1 existe déjà
        Nom_field.clear(); // Efface le champ nom
        Nom_field.sendKeys("Calendrier - Test Calendrier Dérivé"); // Saisit un nouveau nom
        if (!genererlecode_checkbox.isSelected()) {
            genererlecode_checkbox.click(); // Coche la case s'il n'est pas coché
        }
        EnregistrerButton.click(); // Enregistre le calendrier
        assertTrue("le dérivé n'est pas créé", msg_enregistre.isDisplayed()); // Vérifie que le calendrier dérivé est bien créé
        reduire_derive.click(); // Réduit l'arbre du calendrier dérivé
        WebDriverWait wait = new WebDriverWait(driver, 2);
        String locator_derive_cal = "//span[@class='z-label' and text()='Calendrier - Test Calendrier Dérivé']";
        boolean isElementInvisible = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator_derive_cal)));
        Assert.assertTrue("L'élément est toujours visible.", isElementInvisible); // Vérifie que l'élément est bien disparu
    }

    // Méthode pour copier un calendrier
    public void copieCal(WebDriver driver) {

        Cree_copieCal.click(); // Clique sur le bouton pour copier un calendrier
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement Nom_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[substring(@id, string-length(@id) - 1) = '45']")));
        Nom_field.clear(); // Efface le champ nom
        Nom_field.sendKeys("Calendrier - Test 1"); // Saisit un nouveau nom pour la copie
        assertTrue("le nom de type n'est pas correct", type_cal.isDisplayed());
        EnregistrerButton.click();
        assertTrue("la copie n'est pas bien créée", driver.findElement(By.xpath("//span[@class='z-label' and text()='Calendrier - Test 1']")).isDisplayed()); // Vérifie la création de la copie

        // Créer un calendrier par copie - Nom du calendrier non conforme
        Cree_copieCal2.click(); // Clique sur le bouton pour créer une autre copie
        EnregistrerButton.click();
        assertTrue("message 'Calendrier -Test 1 existe déjà' manquant", msg_copie_existe_deja.isDisplayed()); // Vérifie le message d'erreur

        // Créer un 2ᵉ calendrier par copie
        Nom_field.clear();
        Nom_field.sendKeys("Calendrier - Test 2"); // Saisit un nouveau nom pour la copie
        assertTrue("le nom de type n'est pas correct", type_cal.isDisplayed());
        EnregistrerButton.click();
        assertTrue("la copie n'est pas bien créée", msgcaltest2enregistre.isDisplayed()); // Vérifie la création de la copie
    }
    public void ajouterException(WebDriver driver){

        modif_cal_test1.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement bouton_creeexception = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@class='z-button-cm' and text()='Créer une exception']")));
        cree_exception.click();

        assertEquals("Merci de choisir un type d'exception",msg_erreur_exception.getText());
        driver.findElement(By.xpath("//i[@class='z-combobox-btn']")).click();
        driver.findElement(By.xpath("//td[@class='z-comboitem-text'and text()='STRIKE']")).click();
        cree_exception.click();
        assertEquals("Merci de choisir une date de fin pour l'exception",msg_erreur_exception.getText());


        // Obtenez la date actuelle
        LocalDate currentDate = LocalDate.now();
        // Formatez la date dans le format souhaité
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM. yyyy", Locale.FRENCH);
        String formattedDate = currentDate.format(formatter);
        WebElement dateInput = driver.findElement(By.xpath("//input[@class='z-datebox-inp z-datebox-text-invalid']"));
        dateInput.sendKeys(formattedDate);
        cree_exception.click();
        assertTrue(ligne_exception.isDisplayed());

        //String formattedDate = "2 janv. 2024";
        DateTimeFormatter firstFormatter = DateTimeFormatter.ofPattern("d MMM. yyyy", Locale.FRENCH);
        // Seconde date en format (ISO "2024-01-02")

        String secondDateString = driver.findElement(By.xpath("//fieldset[1]/div[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[1]/div[1]/span[@class='z-label']")).getText();
        DateTimeFormatter secondFormatter = DateTimeFormatter.ISO_LOCAL_DATE;

        // Conversion des chaînes en objets LocalDate
        LocalDate firstDate = LocalDate.parse(formattedDate, firstFormatter);
        LocalDate secondDate = LocalDate.parse(secondDateString, secondFormatter);
        // Vérification de la date de l'exception
        Assert.assertEquals(firstDate, secondDate);

        String typeExceptionaVerifier = driver.findElement(By.xpath("//fieldset[1]/div[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[2]/div[1]/span[1]")).getText();
        assertEquals("le type de l'exception est incorrect","STRIKE",typeExceptionaVerifier);

        String effortExceptionaVerifier = driver.findElement(By.xpath("//fieldset[1]/div[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[3]/div[1]/span[1]")).getText();
        assertEquals("le type de l'exception est incorrect","0:0",effortExceptionaVerifier);

        String effortsupExceptionaVerifier = driver.findElement(By.xpath("//fieldset[1]/div[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[4]/div[1]/span[1]")).getText();
        assertEquals("le type de l'exception est incorrect","0:0",effortsupExceptionaVerifier);

       //WebElement code = driver.findElement(By.xpath("//div/input[@class='z-textbox z-textbox-disd z-textbox-text-disd']"));
        WebElement codeExceptionaVerifier = driver.findElement(By.xpath("//div/input[@class='z-textbox z-textbox-disd z-textbox-text-disd']"));

        // Vérifiez si le champ est vide
        String text = codeExceptionaVerifier.getText();
        boolean isEmpty = text.isEmpty();

        // Vérifiez si le champ est non modifiable (désactivé)
        boolean isDisabled = codeExceptionaVerifier.getAttribute("disabled") != null;

        // Assert pour confirmer que le champ est vide et non modifiable
        Assert.assertTrue("Le champ doit être vide", isEmpty);
        Assert.assertTrue("Le champ doit être désactivé", isDisabled);

//
//        WebElement dateverif = driver.findElement(By.xpath("//tbody[1]/tr[3]/td[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[2]/div[1]"));
//        System.out.println("letexte: "+ dateverif.getText());

        assertTrue(type_exceptionstrike.isDisplayed());
        assertTrue(temps_travaille_exception.isDisplayed());

        EnregistrerButton.click();

    }
    public void MettreaJour_chargedeTravail_Exception(WebDriver driver, String nombre_heure_effortNormal, String nombre_minute_effortNormal,String nombre_heure_effortSupp,String nombre_minute_effortSupp) {
        // Assurez-vous que le clic sur modif_cal_test1 est nécessaire et fonctionne comme prévu
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[3]/div[1]/div[3]/table[1]/tbody[2]/tr[2]/td[4]/div[1]//span[3]//img[1]")));
        modif_cal_test1.click();
        heure_effort_normal.sendKeys(nombre_heure_effortNormal);
        minute_effort_normal.sendKeys(nombre_minute_effortNormal);
        heure_effort_supp.sendKeys(nombre_heure_effortSupp);
        minute_effort_supp.click();
        minute_effort_supp.sendKeys(nombre_minute_effortSupp);
        mettre_a_jour_exception_bouton.click();

        String effortExceptionaVerifier = driver.findElement(By.xpath("//fieldset[1]/div[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[3]/div[1]/span[1]")).getText();
        assertEquals("l'effort normal est incorrect",nombre_heure_effortNormal+":"+nombre_minute_effortNormal,effortExceptionaVerifier);
        String effortsupExceptionaVerifier = driver.findElement(By.xpath("//fieldset[1]/div[1]/div[1]/div[3]/table[1]/tbody[2]/tr[1]/td[4]/div[1]/span[1]")).getText();
        assertEquals("l'effort supp est incorrect",nombre_heure_effortSupp+":"+nombre_minute_effortSupp,effortsupExceptionaVerifier);
        EnregistrerButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='z-label' and text()='test1']")));
        WebElement calendrier_cree = driver.findElement(By.xpath("//span[@class='z-label' and text()='test1']"));
        assertTrue("Calendrier Test1 n'est pas créé", calendrier_cree.isDisplayed());

        modif_cal_test1.click();
        String  code_donnee_calendrier = driver.findElement(By.xpath("//td/input[@class='z-textbox z-textbox-disd z-textbox-text-disd']")).getText();
        String  code_liste_exception =  driver.findElement(By.xpath("//div/input[@class='z-textbox z-textbox-disd z-textbox-text-disd']")).getText();
        assertEquals(code_liste_exception,code_donnee_calendrier);

        annulerButton.click();
        CheckOnPageListeCalendriers();
    }
    public void verife_derive_exception(WebDriver driver){

        Cree_Derive.click(); // Clique sur le bouton pour créer un calendrier dérivé
        CheckOnPageCreerCalendrier(); // Vérifie la présence des éléments sur la page de création
        String competent_nom = Nom_field.getAttribute("value");
        Assert.assertTrue("le champ nom n'est pas vide", competent_nom.isEmpty()); // Vérifie que le champ nom est vide
        assertTrue("'Type de Calendrier' est manquant", type_calendrier.isDisplayed()); // Vérifie la présence du type de calendrier
        assertTrue(typeexception_STRIKE.isDisplayed());
        assertTrue(origine_herite.isDisplayed());
        annulerButton.click();
        CheckOnPageListeCalendriers();
        }
    public void verifie_copie_exception(WebDriver driver){
        Cree_copieCal.click();
        assertTrue(typeexception_STRIKE.isDisplayed());
        assertTrue(origine_direct.isDisplayed());
        annulerButton.click();
        CheckOnPageListeCalendriers();
    }
}