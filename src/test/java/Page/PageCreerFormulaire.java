package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageCreerFormulaire {
    @FindBy(xpath ="//input[contains(@id,'k5')]" )
    private WebElement champNom;
    @FindBy (xpath ="//textarea[contains(@id,'n5')]" )
    private WebElement champDescription;
    @FindBy (xpath ="//select[contains(@id,'s5')]" )
    private WebElement champTypeFormulaire;
    @FindBy (xpath ="//input[contains(@id,'v5-real')]" )
    private WebElement champAvancement;
     @FindBy (xpath ="//td[contains(text(),'Nouvel élément du formulaire qualité')]" )
     private WebElement btnNouvelElement;
    @FindBy (xpath ="//div[contains(@id,'16-cave')]" )
    private WebElement colonneNom;
    @FindBy (xpath ="//div[contains(@id,'26-cave')]" )
    private WebElement colonnePosition;
    @FindBy (xpath ="//div[contains(@id,'36-cave')]" )
    private WebElement colonnePourcentage;
    @FindBy (xpath ="//div[contains(@id,'46-cave')]" )
    private WebElement colonneOperations;
    @FindBy (xpath ="//td[contains(text(),'Enregistrer')]" )
    private WebElement btnEnregistrer;
    @FindBy (xpath ="//td[contains(text(),'Sauver et continuer')]" )
    private WebElement btnSauverContinuer;
    @FindBy (xpath ="//td[contains(text(),'Annuler')]" )
    private WebElement btnAnnuler;

    @FindBy (xpath ="//input[contains(@id,'86')]" )
    private WebElement nomNouvelElement;
    @FindBy (xpath ="//span[contains(@id,'96')]" )
    private WebElement positionNouvelElement;
    @FindBy (xpath ="//input[contains(@id,'a6')]" )
    private WebElement pourcentageNouvelElement;
    @FindBy (xpath ="//img[@src='/libreplan/common/img/ico_borrar1.png']" )
    private WebElement optionsNouvelElement;
    @FindBy (xpath ="//span[contains(@id,'w6')]" )
    private WebElement positionUnNouvelElement;
    @FindBy (xpath ="//span[contains(@id,'27')]" )
    private WebElement positionDeuxNouvelElement;
    @FindBy (xpath ="//input[contains(@id,'v6')]" )
    private WebElement nomNouvelElementDeux;
    @FindBy (xpath ="//input[contains(@id,'x6')]" )
    private WebElement pourcentageNouvelElementDeux;
    //p7IQw
    @FindBy (xpath ="//div[contains(@id,'5-cap')]" )
    private WebElement click;
    @FindBy (xpath ="//span[contains(@id,'c6')]" )
    private WebElement postionUn;
    @FindBy (xpath ="//input[contains(@id,'b6')]" )
    private WebElement nomUn;
    @FindBy (xpath ="//span[contains(@id,'p6')]" )
    private WebElement postionDeux;
    @FindBy (xpath ="//input[contains(@id,'m6')]" )
    private WebElement nomDeux;
    @FindBy (xpath ="//span[contains(@id,'v6')]" )
    private WebElement messageFormulairQualite;
    @FindBy (xpath ="//td[contains(@id,'5-cnt')]" )
    private WebElement titleFormulairQualite;
    public String checkTitleFormulairQualite(){
        return titleFormulairQualite.getText();
    }
    public String checkMessageFormulairQualite(){
        return messageFormulairQualite.getText();
    }
    public String elementPositionUn(){
        return nomUn.getAttribute("value")+postionUn.getText() ;
    }
    public String elementPositionDeux(){
        return nomDeux.getAttribute("value")+postionDeux.getText() ;
    }
    public String checkPositionUnNouvelElement(){
        return  positionDeuxNouvelElement.getText();
    }
    public String checkPositionDeuxNouvelElement(){
        return  positionUnNouvelElement.getText();
    }
    public String checkNomNouvelElement(){
        return nomNouvelElement.getText();
    }
    public String checkPositionNouvelElement(){
        return positionNouvelElement.getText();
    }
    public String checkPourcentageNouvelElement(){
        return pourcentageNouvelElement.getText();
    }
    public boolean checkOptionsNouvelElement(){
        return optionsNouvelElement.isDisplayed();
    }
    public String checkChampNom(){
        return champNom.getText();
    }
    public String checkChampDescription(){
        return champDescription.getText();
    }
    public String checkChampTypeFormulaire(){
        return champTypeFormulaire.getText();
    }
    public boolean checkChampAvancement(){
        return champAvancement.isSelected();
    }
    public boolean checkColonneTableau(){
        return colonneNom.isDisplayed() && colonnePourcentage.isDisplayed() && colonnePosition.isDisplayed() && colonneOperations.isDisplayed();
    }
    public boolean checkBtnEnregistrer(){
      return  btnEnregistrer.isDisplayed();
    }
    public boolean checkBtnSauverContinuer(){
        return  btnSauverContinuer.isDisplayed();
    }
    public boolean checkBtnAnnuler(){
        return  btnAnnuler.isDisplayed();
    }
    public void renseignerFormulaire(String nom, String description) {
        champNom.sendKeys(nom);
        champDescription.sendKeys(description);
        champAvancement.click();
        btnNouvelElement.click();
    }
    public void creerFormulaireQualite(String nom,String pourcentage) {
        nomNouvelElement.sendKeys(nom);
        pourcentageNouvelElement.sendKeys(pourcentage);
        btnNouvelElement.click();
    }
    public void creerFormulaireQualite1(String nom,String pourcentage){
        nomNouvelElementDeux.sendKeys(nom);
        pourcentageNouvelElementDeux.sendKeys(pourcentage);
        click.click();
    }
    public void clickContinuerSauver(){
        champAvancement.click();
        btnSauverContinuer.click();
    }
    public PageFormulaireQualite clickBtnAnnuler(WebDriver driver){
        btnAnnuler.click();
        return PageFactory.initElements(driver,PageFormulaireQualite.class);
    }
}
