package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class PageCreateCriteria {
    @FindBy(xpath = "//td[contains(text(),'Créer Type de critère')]")
    WebElement title;
    @FindBy(xpath = "//span[contains(@class,'z-tab-text')]")
    WebElement formTitle;
    @FindBy(xpath = "//span[contains(@class,'save-button')]")
    WebElement saveButton;
    @FindBy(xpath = "//span[contains(@class,'saveandcontinue-button')]")
    WebElement saveContinueButton;
    @FindBy(xpath = "//span[contains(@class,'cancel-button')]")
    WebElement cancelButton;
    @FindBy(xpath ="//input[contains(@id,'e5')]")
    WebElement nameInput;
    @FindBy(xpath ="//i[contains(@id,'h5-btn')]")
    WebElement selectButton;
    @FindBy(xpath = "//textarea[contains(@id,'t5')]")
    WebElement descriptionInput;

    //assert that the elements needed are displayed
    public void checkOnPage(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        assertTrue("Title is missing",title.isDisplayed());
        assertEquals("Form title doesn't match","Modifier",formTitle.getText());
        assertTrue("Save button is missing",saveButton.isDisplayed());
        assertTrue("Save and continue button is missing",saveContinueButton.isDisplayed());
        assertTrue("Cancel button is missing",cancelButton.isDisplayed());
    }
    //fill the create criteria form with name and type
    public void fillForm(String name,String type, WebDriver driver){
        nameInput.clear();
        nameInput.sendKeys(name);
        selectButton.click();
        WebElement selectType = driver.findElement(By.xpath("//td[contains(@class,'z-comboitem-text') and contains(text(),'"+type+"')]"));
        selectType.click();
        descriptionInput.clear();
        descriptionInput.sendKeys(name);
    }

    //click the cancel button
    public PageCriteria cancel(WebDriver driver){
        cancelButton.click();
        return PageFactory.initElements(driver,PageCriteria.class);
    }

    //fill the create criteria form and cancel
    public PageCriteria fillAndCancel(String name,String type,WebDriver driver){
        fillForm(name,type,driver);
        return cancel(driver);
    }

    //fill the create criteria form and save
    public PageCriteria fillAndSave(String name,String type, WebDriver driver){
        fillForm(name,type,driver);
        saveButton.click();
        return PageFactory.initElements(driver,PageCriteria.class);
    }

    //fill the create criteria form and continue(go to the edit page of the criteria)
    public PageEditCriteria fillSaveContinue(String name, String type,WebDriver driver){
        fillForm(name,type,driver);
        saveContinueButton.click();
        return PageFactory.initElements(driver,PageEditCriteria.class);
    }

}
