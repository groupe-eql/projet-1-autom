package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageCreationNouveauProjet {
    @FindBy (xpath = "//input[contains(@id,'p7')]")
    private WebElement getNameProjet;

    @FindBy (xpath = "//i[contains(@id,'v7-btn')]")
    private WebElement clickLoupe;

    @FindBy (xpath = "//div[contains(@id,'y7-body')]")
    private WebElement getList;

    @FindBy (xpath = "//input[contains(@id,'38')]")
    private WebElement codegrise;

    @FindBy (xpath = "//input[contains(@id,'48-real')]")
    private WebElement checkBoxSelected;

    public String checkNameProject(){
        return getNameProjet.getText();
    }
    public String checkListLoupe(){
        clickLoupe.click();
         return getList.getText();
    }
    public String checkChampsGrise(){
        clickLoupe.click();
        return codegrise.getAttribute("class");
    }
    public boolean checkBoxSelected(){
        return checkBoxSelected.isSelected();
    }

    @FindBy (xpath = "//i[contains(@id,'s9-btn')]")
    private WebElement clickLoupeClient;
    @FindBy (xpath = "//div[contains(@id,'v9-body')]")
    private WebElement getListClient;

    public String checkListLoupeClient(){
        clickLoupeClient.click();
        return getListClient.getText();

    }
    @FindBy (xpath = "//td[contains(text(),'Accepter')]")
    private WebElement AccepterBtn;

    public boolean btnAccepter(){
        clickLoupeClient.click();
        return AccepterBtn.isDisplayed();
    }
    @FindBy (xpath = "//input[contains(@id,'k9-real')]")
    private WebElement checkDate;
    @FindBy (xpath = "//input[contains(@id,'n9-real')]")
    private WebElement checkEcheance;
    @FindBy (xpath = "//input[contains(@id,'0a-real')]")
    private WebElement checkCalendrier;
    @FindBy (xpath = "//td[contains(text(),'Détail du projet')]")
    private WebElement checkDetailProject;
    @FindBy (xpath = "//td[contains(text(),'Planification de projet')]")
    private WebElement checkPlanification;
    @FindBy (xpath = "//td[contains(text(),'Chargement des ressources')]")
    private WebElement checkChargementRessources;
    @FindBy (xpath = "//td[contains(text(),'Allocation avancée')]")
    private WebElement checkAllocationAvancée;
    @FindBy (xpath = "//td[contains(text(),'Tableau de bord')]")
    private WebElement checkTableauBord;
    @FindBy (xpath = "//span[contains(text(),'WBS (tâches)')]")
    private WebElement checkWBS;
    public boolean checkWBS(){
        return checkWBS.isDisplayed();
    }
    public boolean checkMenuVertical(){
       return  checkTableauBord.isDisplayed() && checkAllocationAvancée.isDisplayed() && checkChargementRessources.isDisplayed()
        && checkPlanification.isDisplayed() && checkDetailProject.isDisplayed();
    }

    public boolean checkDetailProject(){
       return checkDetailProject.isDisplayed();
    }
    public String checkCalendrier(){
        return checkCalendrier.getAttribute("value");
    }
    public String checkEcheance(){
        return checkEcheance.getText();
    }
    public String checkDate(){
        return  checkDate.getAttribute("value");
    }
    public void remplirChamps(String name,String code,String date,String Échéance){
        getNameProjet.sendKeys(name);
        checkBoxSelected.click();
        codegrise.clear();
        codegrise.sendKeys(code);
        checkDate.clear();
        checkDate.sendKeys(date);
        checkEcheance.sendKeys(Échéance);
        AccepterBtn.click();
    }
    @FindBy (xpath = "//td[contains(text(),'Annuler')]")
    private WebElement AnnulerBtn;
    public boolean btnAnnuler(){
        return AnnulerBtn.isDisplayed();
    }
    @FindBy(xpath = "//div[contains(@id,'3c-hm')]")
    private WebElement btn_WBS;
    @FindBy(xpath = "//div[contains(@id,'4c-hm')]")
    private WebElement btn_DonnéesGeneral;
    @FindBy(xpath = "//div[contains(@id,'5c-hm')]")
    private WebElement cout;
    @FindBy(xpath = "//div[contains(@id,'6c-hm')]")
    private WebElement avancement;
    @FindBy(xpath = "//div[contains(@id,'7c-hm')]")
    private WebElement livelles;
    @FindBy(xpath = "//div[contains(@id,'8c-hm')]")
    private WebElement exigence;
    @FindBy(xpath = "//div[contains(@id,'9c-hm')]")
    private WebElement matriels;
    @FindBy(xpath = "//div[contains(@id,'ac-hm')]")
    private WebElement formulaire;
    @FindBy(xpath = "//div[contains(@id,'bc-hm')]")
    private WebElement autorisation;

    public boolean checkMenuHorizontal(){
        return (btn_WBS.isDisplayed() && btn_DonnéesGeneral.isDisplayed() && cout.isDisplayed() && livelles.isDisplayed()
                && exigence.isDisplayed() && matriels.isDisplayed() && formulaire.isDisplayed() && autorisation.isDisplayed());
    }

    @FindBy (xpath = "//img[@src='/libreplan/common/img/ico_save.png']")
    private WebElement enregistrement;
    @FindBy(xpath = "//td/table/tbody/tr/td/table/tbody/tr/td[1]/span/table/tbody/tr[2]/td[2]/img")
    private WebElement enreg;
    @FindBy (xpath = "//img[@src='/libreplan/common/img/ico_back.png']")
    private WebElement annulerEdition;
    @FindBy (xpath = "//td/table/tbody/tr/td/table/tbody/tr/td[3]/span/table/tbody/tr[2]/td[2]/img")
    private WebElement annuler;
    @FindBy(xpath = "span[conatins(@id,'g8')]")
    private WebElement popup_annulation;
    @FindBy (xpath = "//table[2]/tbody/tr/td/table/tbody/tr/td[3]/span/table/tbody/tr[2]/td[@class='z-button-cm' and text()='Annuler']")
    private WebElement clickAnnuler;
    @FindBy (xpath = "//table[1]/tbody/tr/td/table/tbody/tr/td[3]/div/span")
    private WebElement textAnnuler;
    @FindBy (xpath = "//table[2]/tbody/tr/td/table/tbody/tr/td[1]/span/table/tbody/tr[2]/td[2]")
    private WebElement btn_OK;


    public boolean checkBtnEnregistrement(WebDriver driver){
        Actions a = new Actions(driver);
        a.moveToElement(enregistrement).build().perform();
       return enreg.isDisplayed();
    }
    public boolean checkBtnAnuulerEdition(WebDriver driver){
        Actions a = new Actions(driver);
        a.moveToElement(annulerEdition).build().perform();
       return annuler.isDisplayed();
    }
    public String annulationEditionProjet1()  {
        annulerEdition.click();
     return   textAnnuler.getText();
    }

    public boolean clickAnnuler(){
        clickAnnuler.click();
       return btn_WBS.isDisplayed() && checkDetailProject.isDisplayed();
    }
//    public boolean clickOK() throws InterruptedException {
//        annulerEdition.click();
//        btn_OK.click();
//        Thread.sleep(2000);
//        return checkPlanification.isDisplayed();
//    }
    @FindBy (xpath = "//button[contains(@id,'7-b')]")
    private WebElement btnCalendrier;
    @FindBy(xpath = "//a[@href='/libreplan/planner/index.zul;orders_list']")
    private WebElement btn_projects;

    public void checkProjects(WebDriver driver){
        Actions a = new Actions(driver);
        a.moveToElement(btnCalendrier).build().perform();
        btn_projects.click();
    }
    @FindBy (xpath = "//span[contains(@id,'67')]")
    private WebElement nomProjet;
    @FindBy (xpath = "//span[contains(@id,'77')]")
    private WebElement codeProjet;
    @FindBy (xpath = "//span[contains(@id,'87')]")
    private WebElement dateProjet;
    @FindBy (xpath = "//span[contains(@id,'97')]")
    private WebElement echeanceProjet;
    @FindBy (xpath = "//span[contains(@id,'a7')]")
    private WebElement clientProjet;
    @FindBy (xpath = "//span[contains(@id,'b7')]")
    private WebElement budgetProjet;
    @FindBy (xpath = "//span[contains(@id,'c7')]")
    private WebElement heuresProjet;
    @FindBy (xpath = "//span[contains(@id,'d7')]")
    private WebElement etatProjet;
    public String checkNomProjet(){
       return nomProjet.getText();
    }
    public String checkCodeProjet(){
        return codeProjet.getText();
    }
    public String checDateProjet(){
        return dateProjet.getText();
    }
    public String checkEcheanceProjet(){
        return echeanceProjet.getText();
    }
    public String checkBudgetProjet(){
        return budgetProjet.getText();
    }
    public String checkClientProjet(){
        return clientProjet.getText();
    }

    public String checkHeuresProjet(){
        return heuresProjet.getText();
    }
    public String checkEtatProjet(){
        return etatProjet.getText();
    }

    @FindBy (xpath = "//span[contains(@id,'f7')]")
    private WebElement iconeModifier;
    @FindBy (xpath = "//span[contains(@id,'g7')]")
    private WebElement iconeSupprimer;
    @FindBy (xpath = "//span[contains(@id,'h7')]")
    private WebElement iconePrevision;
    @FindBy (xpath = "//span[contains(@id,'i7')]")
    private WebElement iconeCreer;

    public String checkModifierProjet(){
        return iconeModifier.getAttribute("title");
    }
    public String checkSupprimerProjet(){
        return iconeSupprimer.getAttribute("title");
    }
    public String checkPrevisionProjet(){
        return iconePrevision.getAttribute("title");
    }
    public String checkCreerProjet(){
        return iconeCreer.getAttribute("title");
    }

    @FindBy (xpath ="//img[@src='/libreplan/common/img/ico_borrar1.png']" )
    private WebElement delete;

    @FindBy (xpath ="//td[contains(text(),'OK')]" )
    private WebElement ok;
    @FindBy (xpath ="//span[contains(text(),'PROJET_TEST1 supprimé')]" )
    private WebElement supp;
    public void delete(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(delete));
        delete.click();
        WebDriverWait wait1=new WebDriverWait(driver,10);
        wait1.until(ExpectedConditions.visibilityOf(ok));
        ok.click();
        WebDriverWait wait2=new WebDriverWait(driver,10);
        wait2.until(ExpectedConditions.visibilityOf(supp));
    }



}
