package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageLogin {
    @FindBy (name = "j_username")
    private WebElement username;
    @FindBy (name = "j_password")
    private WebElement password;
    @FindBy (xpath = "//input[@type='submit']")
    private WebElement submit;

    public PageHome login(WebDriver driver, String user, String pass){
        username.clear();
        username.sendKeys(user);
        password.clear();
        password.sendKeys(pass);
        submit.click();
    return PageFactory.initElements(driver, PageHome.class);}
}
