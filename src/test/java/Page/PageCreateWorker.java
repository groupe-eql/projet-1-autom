package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PageCreateWorker {
    @FindBy (xpath = "//span[contains(text(),'Prénom')]/ancestor::tr//input")
    WebElement firstNameInput;
    @FindBy (xpath = "//span[contains(text(),'Nom') and contains(@id,'t6')]/ancestor::tr//input")
    WebElement lastNameInput;
    @FindBy (xpath = "//span[contains(text(),'ID')]/ancestor::tr//input")
    WebElement idInput;
    @FindBy(xpath = "//span[contains(@class,'save-button') and contains(@id,'7f')]")
    WebElement saveButton;
    @FindBy(xpath = "//span[contains(@class,'saveandcontinue-button') and contains(@id,'8f')]")
    WebElement saveAndContinueButton;
    @FindBy(xpath = "//span[contains(@class,'cancel-button') and contains(@id,'9f')]")
    WebElement cancelButton;
    @FindBy(xpath = "//span[text()='Données personnelles']")
    WebElement personalData;
    @FindBy(xpath = "//span[text()='Données personnelles']/ancestor::li")
    WebElement personalDataClass;
    @FindBy(xpath = "//span[text()='Données de base']")
    WebElement baseData;
    @FindBy(xpath = "//input[contains(@id,'k6')]")
    WebElement codeInput;
    @FindBy(xpath = "//input[contains(@id,'6-real')]")
    WebElement codeCheckbox;
    @FindBy(xpath = "//span[contains(text(),'Type')]/ancestor::tr//select[contains(@id,'17') and contains(@style,'200')]")
    WebElement typeInput;
    @FindBy(xpath = "//span[text()='Utilisateur lié']")
    WebElement linkedUser;
    @FindBy(xpath = "//label[contains(@class,'z-radio-cnt') and contains(text(),'Non lié')]")
    WebElement notLinked;
    @FindBy(xpath = "//label[contains(@class,'z-radio-cnt') and contains(text(),'Utilisateur existant')]")
    WebElement existingUser;
    @FindBy(xpath = "//label[contains(@class,'z-radio-cnt') and contains(text(),'Créer un nouvel utilisateur')]")
    WebElement newUser;
    @FindBy(xpath = "//span[contains(text(),'utilisateur')]/ancestor::tr//input")
    WebElement usernameInput;
    @FindBy(xpath = "//span[contains(text(),'Mot de passe')]/ancestor::tr//input")
    WebElement passwordInput;
    @FindBy(xpath = "//span[contains(text(),'Confirmation du mot de passe')]/ancestor::tr//input")
    WebElement passwordConfimInput;
    @FindBy(xpath = "//span[contains(text(),'Email')]/ancestor::tr//input")
    WebElement emailInput;


    public void checkOnPersonalData(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(personalData));
        String tabClass=personalDataClass.getAttribute("class");
        assertTrue("Tab isn't selected",tabClass.contains("z-tab-seld"));
        assertTrue("Données de base missing",baseData.isDisplayed());
        assertFalse("Code input isn't disabled",codeInput.isEnabled());
        assertTrue("Code input missing",codeInput.isDisplayed());
        assertTrue("Code checkbox is missing",codeCheckbox.isDisplayed());
        assertTrue("Code checkbox isn't selected",codeCheckbox.isSelected());
        assertTrue("Prenom input missing",firstNameInput.isDisplayed());
        assertEquals("Prenom input not empty","",firstNameInput.getText());
        assertTrue("Nom input missing",lastNameInput.isDisplayed());
        assertEquals("Nom input not empty","",lastNameInput.getText());
        assertTrue("ID input missing",idInput.isDisplayed());
        assertEquals("ID input not empty","",idInput.getText());
        Select typeSelect=new Select(typeInput);
        assertTrue("type select is missing",typeInput.isDisplayed());
        assertEquals("Wrong default type","Ressource normale",typeSelect.getFirstSelectedOption().getText());
        assertTrue("Utilisateur lié missing",linkedUser.isDisplayed());
        assertTrue("Non lié missing",notLinked.isDisplayed());
        assertTrue("Utilisateur existant missing",existingUser.isDisplayed());
        assertTrue("Créer un nouvel utilisateur missing",newUser.isDisplayed());
        String notLinkedId=notLinked.getAttribute("for");
        WebElement radioNotLinked=driver.findElement(By.id(notLinkedId));
        assertTrue("Radio button unchecked",radioNotLinked.isSelected());
        assertTrue("save button missing",saveButton.isDisplayed());
        assertTrue("save and continue button missing",saveAndContinueButton.isDisplayed());
        assertTrue("cancel button missing",cancelButton.isDisplayed());
    }

    public void fillWorkerData(String firstName, String lastName, String id,WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(firstNameInput));
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        idInput.clear();
        idInput.sendKeys(id);
    }
    public PageWorkers addWorker(String firstName, String lastName, String id, WebDriver driver){
        fillWorkerData(firstName,lastName,id,driver);
        saveButton.click();
        return PageFactory.initElements(driver,PageWorkers.class);
    }

    public PageWorkers addWorkerAndUser(String firstName, String lastName, String id, String userName, String passWord, String email, WebDriver driver){
        fillWorkerData(firstName,lastName,id,driver);
        addLinkedUser(userName,passWord,email,driver);
        saveButton.click();
        return PageFactory.initElements(driver,PageWorkers.class);
    }

    public void addLinkedUser(String userName, String passWord, String email,WebDriver driver){
        String newUserId=newUser.getAttribute("for");
        WebElement radioNewUser=driver.findElement(By.id(newUserId));
        radioNewUser.click();
        assertTrue("Radio button unchecked",radioNewUser.isSelected());
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(usernameInput));
        usernameInput.clear();
        usernameInput.sendKeys(userName);
        passwordInput.clear();
        passwordInput.sendKeys(passWord);
        passwordConfimInput.clear();
        passwordConfimInput.sendKeys(passWord);
        emailInput.clear();
        emailInput.sendKeys(email);
    }



}
