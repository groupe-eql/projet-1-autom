package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;


public class PageHome {
    @FindBy(xpath = "//button[contains(text(),'Calendrier')]")
    private WebElement calendrier;
    @FindBy(xpath = "//button[contains(text(),'Rapports')]")
    private WebElement rapports;
    @FindBy(id = "z_ddstkup")
    private WebElement idFrame;
    @FindBy(xpath = "//a[@href='/libreplan/reports/completedEstimatedHoursPerTask.zul']")
    private WebElement point;
    @FindBy(xpath = "//button[contains(@id,'r-b')]")
    WebElement ressourceButton;
    @FindBy(xpath = "//button[contains(@id,'0-b')]")
    WebElement costButton;
    @FindBy(xpath = "//a[contains(@id,'y-a')]")
    WebElement criteriaButton;
    @FindBy(xpath = "//a[contains(@id,'t-a')]")
    WebElement workerButton;
    @FindBy(xpath = "//a[contains(@id,'r0-a')]")
    WebElement timeSheetButton;

    @FindBy(xpath = "//div[contains(@id,'-cap') and contains(text(),'Mon tableau de bord')]")
    WebElement userTitle;
    @FindBy(xpath = "//a[text()='[Déconnexion]']")
    WebElement logout;

    private WebDriver driver;
    @FindBy(xpath = "//button[@type='button' and text()='Ressources ']")
    public WebElement list_ressource;
    @FindBy(xpath = "//a[@class='z-menu-item-cnt' and @href='/libreplan/calendars/calendars.zul']")
    public WebElement item_calendar;
    @FindBy (xpath = "//img[@src='/libreplan/common/img/ico_add.png']")
    private WebElement creationProjet;
    @FindBy (xpath = "//img[@src='/libreplan/common/img/logo.png']")
    private WebElement goToHomePage;

    @FindBy(xpath = "//td[contains(text(),'Liste des projets')]")
    private WebElement listProjects;
    @FindBy (xpath = "//a[@href='/libreplan/advance/advanceTypes.zul']")
    private WebElement typeAvancement;
    @FindBy (xpath = "//a[@href='/libreplan/qualityforms/qualityForms.zul']")
    private WebElement formulaireQualite;
    public PageFormulaireQualite goToFormulaireQualite(WebDriver driver){
        Actions a=new Actions(driver);
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(ressourceButton));
        a.moveToElement(ressourceButton).perform();
        wait.until(ExpectedConditions.elementToBeClickable(formulaireQualite));
        a.moveToElement(formulaireQualite).perform();
        formulaireQualite.click();
        return PageFactory.initElements(driver, PageFormulaireQualite.class);
    }
    public PageTypeAvancement goToTypeAvancement(WebDriver driver){
        Actions a=new Actions(driver);
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(ressourceButton));
        a.moveToElement(ressourceButton).perform();
        wait.until(ExpectedConditions.elementToBeClickable(typeAvancement));
        a.moveToElement(typeAvancement).perform();
        typeAvancement.click();
        return PageFactory.initElements(driver, PageTypeAvancement.class);
    }


    public boolean calendrierDisplayed() {
        return calendrier.isDisplayed();
    }
    public PageCriteria goToCriteria(WebDriver driver){
        assert  calendrierDisplayed();
        Actions a=new Actions(driver);
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(ressourceButton));
        a.moveToElement(ressourceButton).perform();
        wait.until(ExpectedConditions.elementToBeClickable(criteriaButton));
        a.moveToElement(criteriaButton).perform();
        criteriaButton.click();
        return PageFactory.initElements(driver, PageCriteria.class);
    }
    public PageHeureEstimeesPlanifieesParTache accesItem(WebDriver driver) {
        Actions a = new Actions(driver);
        a.moveToElement(rapports).build().perform();
        point.click();
        return PageFactory.initElements(driver, PageHeureEstimeesPlanifieesParTache.class);
    }


    public PageCalendrier HoverAndSelectListItem(WebDriver driver){
        Actions actions=new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(ressourceButton));
        actions.moveToElement(ressourceButton).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(item_calendar));
        item_calendar.click();
        return  PageFactory.initElements(driver, PageCalendrier.class);
    }

    public PageWorkers goToWorkers(WebDriver driver){
        Actions a = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(ressourceButton));
        a.moveToElement(ressourceButton).perform();
        wait.until(ExpectedConditions.elementToBeClickable(workerButton));
        a.moveToElement(workerButton).perform();
        workerButton.click();
        return PageFactory.initElements(driver,PageWorkers.class);
    }
    public void checkOnWorker(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(userTitle));
        assertTrue("Title missing",userTitle.isDisplayed());
    }
    public PageLogin logout(WebDriver driver){
        logout.click();
        return PageFactory.initElements(driver,PageLogin.class);
    }


    public PageTimeSheets goToTimeSheets(WebDriver driver){
        Actions a = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(costButton));
        a.moveToElement(costButton).perform();
        wait.until(ExpectedConditions.visibilityOf(timeSheetButton));
        a.moveToElement(timeSheetButton).perform();
        timeSheetButton.click();
        return PageFactory.initElements(driver,PageTimeSheets.class);
    }
    public PageCreationNouveauProjet goToCreationProjet(WebDriver driver){
        creationProjet.click();
        return PageFactory.initElements(driver,PageCreationNouveauProjet.class);
    }
    public PageHome goToHomePage(){
        goToHomePage.click();
        return this;
    }


}
