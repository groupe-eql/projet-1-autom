package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageModifierFormulaireQualite {
    @FindBy(xpath ="//td[contains(@id,'5-cnt')]" )
    private WebElement titleModifierFormulaire;
    @FindBy(xpath ="//select[contains(@id,'s5')]" )
    private WebElement select;
    @FindBy (xpath ="//div[contains(@id,'36-cave')]" )
    private WebElement colonnePourcentage;
    @FindBy (xpath ="//td[contains(text(),'Enregistrer')]" )
    private WebElement btnEnregistrer;
    public PageFormulaireQualite clickBtnEnregistrer(WebDriver driver){
        btnEnregistrer.click();
        return PageFactory.initElements(driver, PageFormulaireQualite.class);
    }
    public boolean checkColonnePourcentage(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.invisibilityOf(colonnePourcentage));
        return colonnePourcentage.isDisplayed();
    }
    public String checkTitleModifierFormulaire(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(titleModifierFormulaire));
        return titleModifierFormulaire.getText();
    }
    public void selectParElement() {
        Select select1 = new Select(select);
        select1.selectByIndex(1);

    }

}
