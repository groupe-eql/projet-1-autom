package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageSheetTemplate {
    @FindBy (xpath = "//td[text()='Créer']")
    WebElement createButton;

    public PageSheetTemplate createTemplate(String name,WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(createButton));
        createButton.click();
        PageCreateSheetTemplate pageCreateSheetTemplate=PageFactory.initElements(driver,PageCreateSheetTemplate.class);
        return pageCreateSheetTemplate.addTemplate(name,driver);
    }

    public PageTimeSheets createSheetFromTemplate(long dateDelta,String ressourceId,String hours,String template,WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(createButton));
        WebElement createSheet=driver.findElement(By.xpath("//span[text()='"+template+"']/ancestor::tr//td[text()='Nouvelle feuille de temps']"));
        createSheet.click();
        PageCreateTimeSheet pageCreateTimeSheet=PageFactory.initElements(driver,PageCreateTimeSheet.class);
        pageCreateTimeSheet.checkOnPage(driver);
        return pageCreateTimeSheet.addLineNotDone(dateDelta,ressourceId,hours,driver);
    }

}
