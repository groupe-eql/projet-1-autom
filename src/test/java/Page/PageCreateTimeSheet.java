package Page;

import Tools.Tools;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.junit.Assert.*;

public class PageCreateTimeSheet {
    @FindBy (xpath = "//td[text()='Enregistrer']")
    WebElement saveButton;
    @FindBy(xpath = "//div[contains(@id,'-cap') and contains(text(),'Créer la feuille de temps')]")
    WebElement title;
    @FindBy(xpath = "//span[text()='Données générales']")
    WebElement generalData;
    @FindBy(xpath = "//span[text()='Champs Rubriques']")
    WebElement headingFields;
    @FindBy(xpath = "//span[text()='Lignes de feuille de temps']")
    WebElement timeSheetLines;
    @FindBy(xpath = "//span[contains(text(),'Code')]/ancestor::tr//input[contains(@id,'6') and contains(@class,'disd')]")
    WebElement codeInput;
    @FindBy(xpath = "//input[@type='checkbox']")
    WebElement codeCheckbox;
    @FindBy(xpath = "//div[text()='Date' and contains(@id,'-cave')]")
    WebElement dateCol;
    @FindBy(xpath = "//div[text()='Ressource' and contains(@id,'-cave') and contains(@class,'z-column')]")
    WebElement ressourceCol;
    @FindBy(xpath = "//div[contains(text(),'Tâche') and contains(@id,'-cave') and contains(@class,'z-column')]")
    WebElement taskCol;
    @FindBy(xpath = "//div[text()='Heures' and contains(@id,'-cave')]")
    WebElement hourCol;
    @FindBy(xpath = "//div[contains(text(),'Type d') and contains(@id,'-cave')]")
    WebElement hourTypeCol;
    @FindBy(xpath = "//td[text()='Ajouter une ligne']")
    WebElement addLine;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//input[contains(@class,'datebox')]")
    WebElement dateInput;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//input[contains(@class,'combobox')]")
    WebElement ressourceInput;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//input[contains(@class,'bandbox')]")
    WebElement taskInput;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//i[contains(@class,'bandbox') and contains(@id,'btn')]")
    WebElement taskSearchIcon;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//input[contains(@class,'textbox')]")
    WebElement hourInput;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//select")
    WebElement typeInput;

    @FindBy(xpath = "//tr[contains(@class,'listWork')]//input[@type='checkbox']")
    WebElement realisedInput;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//input[@disabled]")
    WebElement addCodeInput;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//span[contains(@title,'Supprimer')]")
    WebElement deleteLine;
    @FindBy(xpath = "//tr[contains(@class,'listWork')]//i[contains(@class,'combobox') and contains(@id,'btn')]")
    WebElement selectRessourceButton;
    @FindBy(xpath = "//div[contains(@class,'shadow')]//div[contains(@class,'z-listbox-body')]//div[contains(text(),'PROJET')]")
    WebElement firstTask;
    public void checkOnPage(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        assertTrue("Title missing",title.isDisplayed());
        assertTrue("Save button missing",saveButton.isDisplayed());
        assertTrue("données générales missing",generalData.isDisplayed());
        assertTrue("code input missing",codeInput.isDisplayed());
        assertFalse("code input isn't disabled",codeInput.isEnabled());
        assertTrue("code checkbox missing",codeCheckbox.isDisplayed());
        assertTrue("code checkbox isn't checked",codeCheckbox.isSelected());
        assertTrue("Champs rubriques missing",headingFields.isDisplayed());
        assertTrue("Lignes de feuille de temps missing",timeSheetLines.isDisplayed());
        assertTrue("Date column missing",dateCol.isDisplayed());
        assertTrue("Ressource column missing",ressourceCol.isDisplayed());
        assertTrue("Tache column missing",taskCol.isDisplayed());
        assertTrue("Heures column missing",hourCol.isDisplayed());
        assertTrue("Type d'heures column missing",hourTypeCol.isDisplayed());
    }
    public PageTimeSheets saveTimeSheet(WebDriver driver){
        saveButton.click();
        return PageFactory.initElements(driver,PageTimeSheets.class);
    }

    public PageTimeSheets addLine(long dateDelta,String ressourceId,String hours,WebDriver driver){
        addLine.click();
        checkAddLine(driver);
        String date=Tools.getFormattedDate(dateDelta);
        dateInput.clear();
        dateInput.sendKeys(date);
        selectRessourceButton.click();
        By selectedRessourceBy=By.xpath("//div[contains(@class,'shadow')]//td[contains(@class,'z-comboitem') and contains(text(),'("+ressourceId+")')]");
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(selectedRessourceBy));
        WebElement selectedRessource=driver.findElement(selectedRessourceBy);
        selectedRessource.click();
        taskSearchIcon.click();
        wait.until(ExpectedConditions.visibilityOf(firstTask));
        Actions a=new Actions(driver);
        a.moveToElement(firstTask).perform();
        firstTask.click();
        hourInput.clear();
        hourInput.sendKeys(hours);
        Select typeSelect=new Select(typeInput);
        typeSelect.selectByVisibleText("Overtime");
        realisedInput.click();
        return saveTimeSheet(driver);
    }
    public PageTimeSheets addLineNotDone(long dateDelta,String ressourceId,String hours,WebDriver driver){
        addLine.click();
        checkAddLine(driver);
        String date=Tools.getFormattedDate(dateDelta);
        dateInput.clear();
        dateInput.sendKeys(date);
        selectRessourceButton.click();
        By selectedRessourceBy=By.xpath("//div[contains(@class,'shadow')]//td[contains(@class,'z-comboitem') and contains(text(),'("+ressourceId+")')]");
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(selectedRessourceBy));
        WebElement selectedRessource=driver.findElement(selectedRessourceBy);
        selectedRessource.click();
        taskSearchIcon.click();
        wait.until(ExpectedConditions.visibilityOf(firstTask));
        Actions a=new Actions(driver);
        a.moveToElement(firstTask).perform();
        firstTask.click();
        hourInput.clear();
        hourInput.sendKeys(hours);
        Select typeSelect=new Select(typeInput);
        typeSelect.selectByVisibleText("Overtime");
        return saveTimeSheet(driver);
    }

    public void checkAddLine(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(dateInput));
        assertTrue("Date input missing",dateInput.isDisplayed());
        String formattedDate = Tools.getFormattedDate(0);
        assertEquals("Date doesn't match",formattedDate,dateInput.getAttribute("value"));
        assertTrue("Ressource input missing",ressourceInput.isDisplayed());
        assertTrue("Task input missing",taskInput.isDisplayed());
        assertTrue("Task search icon missing",taskSearchIcon.isDisplayed());
        assertTrue("Hour input missing",hourInput.isDisplayed());
        assertEquals("Wrong default hour value","0",hourInput.getAttribute("value"));
        assertTrue("Hour type input missing",typeInput.isDisplayed());
        Select typeSelect=new Select(typeInput);
        assertEquals("Wrong default hour type","Default",typeSelect.getFirstSelectedOption().getText());
        assertTrue("Realised input missing",realisedInput.isDisplayed());
        assertFalse("Realised input isn't unchecked",realisedInput.isSelected());
        assertTrue("code input is missing",addCodeInput.isDisplayed());
        assertFalse("code input isn't diabled",addCodeInput.isEnabled());
        assertTrue("Delete button missing",deleteLine.isDisplayed());
    }



}
