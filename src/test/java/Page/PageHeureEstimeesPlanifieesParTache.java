package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageHeureEstimeesPlanifieesParTache {

    @FindBy (xpath = "//div[contains(text(),'Heures estimées')]")
    private WebElement titlepageheure;
    @FindBy (xpath = "//td[contains(text(),'Montrer')]")
    private WebElement buttonMontrer;

    public String checkTitle(){
        return titlepageheure.getText();
    }
    public boolean checkSubtitle(String str, WebDriver driver){
        return driver.findElement(By.xpath("//div[contains(text(),'"+str+"')]")).isDisplayed();
    }
    public boolean checkButtonMontrer(){
        return buttonMontrer.isDisplayed();
    }


}
