package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageFormulaireQualite {
    @FindBy (xpath ="//div[contains(@id,'4-cap')]" )
    private WebElement titleDisplayed;
    @FindBy (xpath ="//div[contains(@id,'u4-cave')]" )
    private WebElement nomFormulaire;
    @FindBy (xpath ="//div[contains(@id,'v4-cave')]" )
    private WebElement descriptionFormulaire;
    @FindBy (xpath ="//div[contains(@id,'w4-cave')]" )
    private WebElement operationsFormulaire;
    @FindBy (xpath ="//input[contains(@id,'o4')]" )
    private WebElement champFiltre;
    @FindBy (xpath ="//td[contains(text(),'Filtre')]" )
    private WebElement btnFiltre;
    @FindBy (xpath ="//td[contains(text(),'Créer')]" )
    private WebElement btnCreer;
    @FindBy (xpath ="//span[contains(@id,'_5')]" )
    private WebElement nomFormulaireTestUn;
    @FindBy (xpath ="//span[contains(@id,'o6')]" )
    private WebElement messageFormulaire;
    @FindBy (xpath ="//span[contains(@id,'l6')]" )
    private WebElement messageEnregistrer;
    @FindBy (xpath ="//img[@src='/libreplan/common/img/ico_borrar1.png']" )
    private WebElement delete;

    @FindBy (xpath ="//td[contains(text(),'OK')]" )
    private WebElement ok;
    public String checkmessegeEnregistrer(){
       return messageEnregistrer.getText();
    }
    public String checkMessageFormulaire(){
        return messageFormulaire.getText();
    }
    public PageModifierFormulaireQualite clickFormulaireTestUn(WebDriver driver){
        nomFormulaireTestUn.click();
        return PageFactory.initElements(driver,PageModifierFormulaireQualite.class);
    }
    public boolean titleDisplayed(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(titleDisplayed));
        return titleDisplayed.isDisplayed();
    }
    public boolean tableauFormulaireDisplayed(){
        return nomFormulaire.isDisplayed() && descriptionFormulaire.isDisplayed() && operationsFormulaire.isDisplayed();
    }
    public boolean champFiltre(){
        return champFiltre.isDisplayed() && btnFiltre.isDisplayed();
    }
    public boolean btnCreerDisplayed(){
        return btnCreer.isDisplayed();
    }
    public PageCreerFormulaire clickBtnCreer(WebDriver driver){
        btnCreer.click();
        return PageFactory.initElements(driver,PageCreerFormulaire.class);
    }
    public void delete(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(delete));
        delete.click();
        WebDriverWait wait1=new WebDriverWait(driver,10);
        wait1.until(ExpectedConditions.visibilityOf(ok));
        ok.click();
    }


}
