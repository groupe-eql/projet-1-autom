package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.*;
public class PageCriteria {
    @FindBy(xpath = "//div[contains(@id,'-cap') and contains(text(),'Types de critères Liste')]")
    WebElement title;
    @FindBy(xpath = "//div[contains(text(),'Nom') and contains(@class,'column')]")
    WebElement nomColumn;
    @FindBy(xpath = "//div[contains(text(),'Code') and contains(@class,'column')]")
    WebElement codeColumn;
    @FindBy(xpath = "//div[contains(text(),'Type') and contains(@class,'column')]")
    WebElement typeColumn;
    @FindBy(xpath = "//div[contains(text(),'Activé') and contains(@class,'column')]")
    WebElement activeColumn;
    @FindBy(xpath = "//div[contains(text(),'Opération') and contains(@class,'column')]")
    WebElement operationColumn;
    @FindBy(xpath = "//td[contains(@class,'z-button-cm') and contains(text(),'Créer')]")
    WebElement button;
    @FindBy(xpath = "//tr[contains(@class,'clickable-rows z-row')]")
    List<WebElement> rows;

    @FindBy(xpath = "//div[contains(@class,'z-window-modal')]//td[text()='Annuler']")
    WebElement cancelDelete;
    @FindBy(xpath = "//div[contains(@class,'z-window-modal')]//td[text()='OK']")
    WebElement okDelete;

    //assert that the elements needed are displayed
    public void checkOnPage(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        assertTrue("Title missing",title.isDisplayed());
        assertTrue("nom missing",nomColumn.isDisplayed());
        assertTrue("code missing",codeColumn.isDisplayed());
        assertTrue("type missing",typeColumn.isDisplayed());
        assertTrue("activé missing",activeColumn.isDisplayed());
        assertTrue("operation missing",operationColumn.isDisplayed());
        assertTrue("button missing",button.isDisplayed());
    }

    //go to the create criteria page
    public PageCreateCriteria createNewCriteria(WebDriver driver){
        button.click();
        return PageFactory.initElements(driver,PageCreateCriteria.class);
    }

    //assert that a criteria is in the list
    public boolean assertRowExists(String name,WebDriver driver){
        try {
            WebElement row=driver.findElement(By.xpath("//span[text()='"+name+"']"));
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
    }

    //get the number of criterias in the list
    public int getNbCriteria(){
        return rows.size();
    }

    //go to the edit page of a criteria by clicking on the edit button
    public PageEditCriteria editCriteria(String name,WebDriver driver){
        WebElement editButton=driver.findElement(By.xpath("//span[text()='"+name+"']/ancestor::tr//span[@title='Modifier']"));
        editButton.click();
        return PageFactory.initElements(driver, PageEditCriteria.class);
    }

    //go to the edit page of a criteria by clicking on it's name
    public PageEditCriteria editCriteriaByName (String name,WebDriver driver){
        WebElement editLink=driver.findElement(By.xpath("//span[text()='"+name+"']"));
        editLink.click();
        return PageFactory.initElements(driver,PageEditCriteria.class);
    }
    //click on the delete button of a criteria and cancel
    public void deleteAndCancel(String name, WebDriver driver){
        WebElement deleteButton=driver.findElement(By.xpath("//span[text()='"+name+"']/ancestor::tr//span[@title='Supprimer']"));
        deleteButton.click();
        checkDeletePopup(name,driver);
        cancelDelete.click();
    }
    //click on the delete button of a criteria and save
    public void deleteAndSave(String name, WebDriver driver){
        By saveboxBy=By.xpath("//span[contains(text(),'Type de critère \""+name+"\" supprimé')]");
        WebElement deleteButton=driver.findElement(By.xpath("//span[text()='"+name+"']/ancestor::tr//span[@title='Supprimer']"));
        deleteButton.click();
        checkDeletePopup(name,driver);
        okDelete.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(saveboxBy));
        WebElement saveBox=driver.findElement(saveboxBy);
        assertTrue("Confirmation missing",saveBox.isDisplayed());
    }
    //assert the delete pop up is displayed
    public void checkDeletePopup(String name,WebDriver driver){
        By textBy=By.xpath("//span[text()='Supprimer Type de critère \""+name+"\". Êtes-vous sûr ?']");
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(textBy));
        WebElement text=driver.findElement(textBy);
        assertTrue("Text missing",text.isDisplayed());
        assertTrue("OK button missing",okDelete.isDisplayed());
        assertTrue("Cancel button missing",cancelDelete.isDisplayed());
    }
}
