package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageCreateSheetTemplate {
    @FindBy(xpath = "//input[contains(@class,'textbox')]")
    WebElement nameInput;
    @FindBy (xpath = "//td[text()='Enregistrer']")
    WebElement saveButton;

    public PageSheetTemplate addTemplate(String name, WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(nameInput));
        nameInput.clear();
        nameInput.sendKeys(name);
        saveButton.click();
        return PageFactory.initElements(driver,PageSheetTemplate.class);
    }
}
