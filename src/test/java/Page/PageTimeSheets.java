package Page;

import Tools.Tools;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.*;

public class PageTimeSheets {
    @FindBy(xpath = "//button[contains(@id,'0-b')]")
    WebElement costButton;
    @FindBy(xpath = "//a[contains(@id,'s0-a')]")
    WebElement templateButton;
    @FindBy (xpath = "//td[text()='Nouvelle feuille de temps']")
    WebElement createButton;
    @FindBy(xpath = "//div[contains(@id,'-cap') and contains(text(),'Liste des feuilles de temps')]")
    WebElement title;
    @FindBy(xpath = "//div[contains(text(),'Date de début') and contains(@class,'column')]")
    WebElement startCol;
    @FindBy(xpath = "//div[contains(text(),'Date de début') and contains(@class,'column')]/parent::th")
    WebElement startCell;
    @FindBy(xpath = "//div[contains(text(),'Date de fin') and contains(@class,'column')]")
    WebElement endCol;
    @FindBy(xpath = "//div[contains(text(),'Modèle') and contains(@class,'column')]")
    WebElement modelCol;
    @FindBy(xpath = "//div[contains(text(),'Travail total') and contains(@class,'column')]")
    WebElement totalWorkCol;
    @FindBy(xpath = "//div[contains(text(),'Code') and contains(@class,'column')]")
    WebElement codeCol;
    @FindBy(xpath = "//div[contains(text(),'Actions') and contains(@class,'column')]")
    WebElement actionsCol;
    @FindBy(xpath = "//span[contains(text(),'Filtrer la feuille de temps par:')]")
    WebElement filterLabel;
    @FindBy(xpath = "//span[contains(text(),'Modèle')]/ancestor::tr//select")
    WebElement filterInput;
    @FindBy(xpath = "//span[contains(text(),'Choisir un canevas:')]/ancestor::tr//select[contains(@id,'j5')]")
    WebElement templateInput;
    @FindBy(xpath = "//span[text()='à']/following::input")
    WebElement endInput;
    @FindBy(xpath = "//span[text()='à']/following::i[contains(@id,'-btn')]")
    WebElement endCalendar;
    @FindBy(xpath = "//span[text()='de']/following::input")
    WebElement startInput;
    @FindBy(xpath = "//span[text()='de']/following::i[contains(@id,'-btn')]")
    WebElement startCalendar;
    @FindBy(xpath = "//td[contains(text(),'Filtre')]")
    WebElement filtreButton;


    public void checkOnPage(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        assertTrue("Title missing",title.isDisplayed());
        assertTrue("Button missing",createButton.isDisplayed());
        assertTrue("Start column missing",startCol.isDisplayed());
        assertTrue("End column missing",endCol.isDisplayed());
        assertTrue("Model column missing",modelCol.isDisplayed());
        assertTrue("Total work column missing",totalWorkCol.isDisplayed());
        assertTrue("Code column missing",codeCol.isDisplayed());
        assertTrue("Actions column missing",actionsCol.isDisplayed());
        assertTrue("Filter input missing",filterInput.isDisplayed());
        assertTrue("Filter label missing",filterLabel.isDisplayed());
        assertTrue("Template input missing",templateInput.isDisplayed());
        Select filterSelect=new Select(filterInput);
        assertEquals("Wrong default filter","Montrer tout",filterSelect.getFirstSelectedOption().getText());
        assertTrue("Start input missing",startInput.isDisplayed());
        assertTrue("End input missing",endInput.isDisplayed());
        assertTrue("Start calendar missing",startCalendar.isDisplayed());
        assertTrue("End calendar missing",endCalendar.isDisplayed());
        assertTrue("Filtre button missing",filtreButton.isDisplayed());
        Select templateSelect=new Select(templateInput);
        assertEquals("Wrong default template","Default",templateSelect.getFirstSelectedOption().getText());

    }
    public PageCreateTimeSheet createTimeSheet(WebDriver driver){
        createButton.click();
        return PageFactory.initElements(driver,PageCreateTimeSheet.class);
    }

    public PageSheetTemplate goToTemplate(WebDriver driver){
        Actions a = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(costButton));
        a.moveToElement(costButton).perform();
        wait.until(ExpectedConditions.visibilityOf(templateButton));
        a.moveToElement(templateButton).perform();
        templateButton.click();
        return PageFactory.initElements(driver,PageSheetTemplate.class);
    }

    public boolean assertRowExists(long dateDelta,String hours,WebDriver driver){
        String date= Tools.getFormattedDate(dateDelta);
        try {
            WebElement dateRow=driver.findElement(By.xpath("//span[text()='"+date+"']/ancestor::tr"));
            WebElement hoursRow=driver.findElement(By.xpath("//span[text()='"+hours+"']/ancestor::tr"));
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public List<LocalDate> getStartDates(WebDriver driver){
        List<LocalDate> dates=new ArrayList<>();
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.FRENCH);
        List<WebElement> elements=driver.findElements(By.cssSelector("tr.clickable-rows>td:first-of-type span"));
        for (WebElement element : elements) {
            dates.add(LocalDate.parse(element.getText(),pattern));
        }
        return dates;
    }
    public List<LocalDate> getEndDates(WebDriver driver){
        List<LocalDate> dates=new ArrayList<>();
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.FRENCH);
        List<WebElement> elements=driver.findElements(By.cssSelector("tr.clickable-rows>td:nth-of-type(2) span"));
        for (WebElement element : elements) {
            dates.add(LocalDate.parse(element.getText(),pattern));
        }
        return dates;
    }

    public boolean assertDateDescending(List<LocalDate> dates){
        for (int i=1;i<dates.size();++i) {
            if(dates.get(i-1).compareTo(dates.get(i))<0){
                return false;
            }
        }
        return true;
    }
    public boolean assertDateAscending(List<LocalDate> dates){
        for (int i=1;i<dates.size();++i) {
            if(dates.get(i-1).compareTo(dates.get(i))>0){
                return false;
            }
        }
        return true;
    }

    public boolean assertNumbersDescending(List<Integer> numbers){
        for (int i=1;i<numbers.size();++i) {
            if(numbers.get(i-1).compareTo(numbers.get(i))<0){
                return false;
            }
        }
        return true;
    }
    public boolean assertNumbersAscending(List<Integer> numbers){
        for (int i=1;i<numbers.size();++i) {
            if(numbers.get(i-1).compareTo(numbers.get(i))>0){
                return false;
            }
        }
        return true;
    }

    public boolean assertStringAscending(List<String> strings){
        for (int i=1;i<strings.size();++i) {
            if(strings.get(i-1).compareTo(strings.get(i))>0){
                return false;
            }
        }
        return true;
    }
    public boolean assertStringDescending(List<String> strings){
        for (int i=1;i<strings.size();++i) {
            if(strings.get(i-1).compareTo(strings.get(i))<0){
                return false;
            }
        }
        return true;
    }

    public PageTimeSheets orderByEndDate(WebDriver driver){
        endCol.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageTimeSheets.class);
    }

    public PageTimeSheets orderByTotalWork(WebDriver driver){
        totalWorkCol.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageTimeSheets.class);
    }

    public PageTimeSheets orderByTemplate(WebDriver driver){
        modelCol.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageTimeSheets.class);
    }
    public List<Integer> getTotalWork(WebDriver driver){
        List<Integer> numbers=new ArrayList<>();
        List<WebElement> elements=driver.findElements(By.cssSelector("tr.clickable-rows>td:nth-of-type(4) span"));
        for (WebElement element : elements) {
            numbers.add(Integer.valueOf(element.getText()));
        }
        return numbers;
    }
    public List<String> getTemplates(WebDriver driver){
        List<String> templates=new ArrayList<>();
        List<WebElement> elements=driver.findElements(By.cssSelector("tr.clickable-rows>td:nth-of-type(3) span"));
        for (WebElement element : elements) {
            templates.add(element.getText());
        }
        return templates;
    }

    public void resizeStartCell(int offset,WebDriver driver){
        Actions a=new Actions(driver);
        a.moveToElement(title).perform();
        int width=startCell.getSize().getWidth();
        System.out.println(width);
        int horizontalSpot=width/2;
        int verticalSpot=(startCell.getSize().getHeight());
        a.moveToElement(startCell).moveByOffset(horizontalSpot,0).build().perform();
        a.clickAndHold().moveByOffset(offset,0).release().build().perform();
        checkOnPage(driver);
        if(offset>0){
            assertTrue(width<startCell.getSize().getWidth());
        } else if (offset<0) {
            assertTrue(width>startCell.getSize().getWidth());
        }

    }


}
