package Page;

import Tools.Tools;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PageWorkers {
    @FindBy (xpath = "//table[contains(@id,'y5-box')]//td[text()='Créer' and @class='z-button-cm']")
    WebElement createButton;
    @FindBy (xpath = "//tr[contains(@class,'clickable-rows')]")
    List<WebElement> workerList;
    @FindBy(xpath = "//div[contains(@id,'-cap') and contains(text(),'Liste des participants')]")
    WebElement title;
    @FindBy(xpath = "//div[contains(text(),'Surnom') and contains(@class,'column')]")
    WebElement surnomCol;
    @FindBy(xpath = "//div[contains(text(),'Prénom') and contains(@class,'column')]")
    WebElement prenomCol;
    @FindBy(xpath = "//div[contains(text(),'ID') and contains(@class,'column')]")
    WebElement idCol;
    @FindBy(xpath = "//div[contains(text(),'Code') and contains(@class,'column')]")
    WebElement codeCol;
    @FindBy(xpath = "//div[contains(text(),'En file') and contains(@class,'column')]")
    WebElement fileCol;
    @FindBy(xpath = "//div[contains(text(),'Opération') and contains(@class,'column')]")
    WebElement operationCol;
    @FindBy(xpath = "//span[contains(text(),'Filtré par')]/ancestor::tr//input[contains(@id,'q4')]")
    WebElement filterInput;
    @FindBy(xpath = "//span[contains(text(),'Détails personnels')]/ancestor::tr//input[contains(@id,'d5')]")
    WebElement detailsInput;
    @FindBy(xpath = "//td[contains(text(),'options')]")
    WebElement optionsButton;
    @FindBy(xpath = "//td[contains(text(),'Filtre')]")
    WebElement filtreButton;
    @FindBy(xpath = "//span[text()='Période active depuis']/following::input")
    WebElement startInput;
    @FindBy(xpath = "//span[text()='Période active depuis']/following::i[contains(@id,'-btn')]")
    WebElement startCalendar;
    @FindBy(xpath = "//span[text()='à']/following::input")
    WebElement endInput;
    @FindBy(xpath = "//span[text()='à']/following::i[contains(@id,'-btn')]")
    WebElement endCalendar;
    @FindBy(xpath = "//span[text()='à']/following::select")
    WebElement typeOption;
    @FindBy(xpath = "//table[contains(@id,'5-next')]//tr")
    WebElement nextButton;
    @FindBy(xpath = "//table[contains(@id,'5-prev')]//tr")
    WebElement previousButton;
    @FindBy(xpath = "//table[contains(@id,'5-last')]//tr")
    WebElement lastButton;
    @FindBy(xpath = "//table[contains(@id,'5-first')]//tr")
    WebElement firstButton;
    @FindBy(xpath = "//input[contains(@class,'z-paging-inp')and contains(@id,'5-real')]")
    WebElement pageInput;
    @FindBy(xpath = "//a[text()='[Déconnexion]']")
    WebElement logout;
    @FindBy(xpath = "//div[contains(@class,'z-window-modal')]//td[text()='OK']")
    WebElement okDeleteWorker;
    @FindBy(xpath = "//div[contains(@class,'z-window-modal')]//td[text()='Oui']")
    WebElement okDeleteUser;

    public void checkOnPage(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        assertTrue("Title missing",title.isDisplayed());
        assertTrue("surnom missing",surnomCol.isDisplayed());
        assertTrue("code missing",codeCol.isDisplayed());
        assertTrue("prénom missing",prenomCol.isDisplayed());
        assertTrue("id missing",idCol.isDisplayed());
        assertTrue("operation missing",operationCol.isDisplayed());
        assertTrue("en file missing",fileCol.isDisplayed());
        assertTrue("button missing",createButton.isDisplayed());
        assertTrue("filter input missing",filterInput.isDisplayed());
        assertTrue("details input is missing",detailsInput.isDisplayed());
        assertTrue("options button missing",optionsButton.isDisplayed());
        assertTrue("filtre button missing",filtreButton.isDisplayed());

    }

    public void createWorkers(String path, WebDriver driver) throws IOException {
        List<Map<String,String>> workers= Tools.loadCSVJDD(path);
        for (Map<String, String> worker : workers) {
            String firstName=worker.get("Prenom");
            String lastName=worker.get("Nom");
            String id=worker.get("ID");
            PageCreateWorker pageCreateWorker=createWorker(driver);
            pageCreateWorker.addWorker(firstName,lastName,id,driver);
        }
    }

    public PageCreateWorker createWorker(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(createButton));
        createButton.click();
        return PageFactory.initElements(driver,PageCreateWorker.class);
    }

    public int getNbWorkers(){
        return workerList.size();
    }
    public boolean assertRowExists(String surname, String firstName, String id,WebDriver driver){
        try {
            WebElement nameRow=driver.findElement(By.xpath("//span[text()='"+surname+"']/ancestor::tr"));
            WebElement firstNameRow=driver.findElement(By.xpath("//span[text()='"+firstName+"']/ancestor::tr"));
            WebElement idRow=driver.findElement(By.xpath("//span[text()='"+id+"']/ancestor::tr"));
            return(nameRow.equals(firstNameRow) && nameRow.equals(idRow));
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public PageWorkers deleteRow(String id, WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        WebElement deleteButton=driver.findElement(By.xpath("//span[text()='"+id+"']/ancestor::tr//span[@title='Supprimer']"));
        deleteButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(okDeleteWorker));
        okDeleteWorker.click();
        wait.until(ExpectedConditions.elementToBeClickable(okDeleteUser));
        okDeleteUser.click();
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }

    public PageWorkers deleteRowWithoutUser(String id, WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        WebElement deleteButton=driver.findElement(By.xpath("//span[text()='"+id+"']/ancestor::tr//span[@title='Supprimer']"));
        deleteButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(okDeleteWorker));
        okDeleteWorker.click();
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }

    public PageWorkers searchByDetail(String value,WebDriver driver){
        detailsInput.clear();
        detailsInput.sendKeys(value);
        filtreButton.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }

    public void moreOptions(WebDriver driver){
        optionsButton.click();
        checkOptions(driver);
    }

    public void checkOptions(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(startInput));
        assertTrue("Start input missing",startInput.isDisplayed());
        assertEquals("Start input isn't empty","",startInput.getText());
        assertTrue("End input missing",endInput.isDisplayed());
        assertEquals("End input isn't empty","",endInput.getText());
        assertTrue("Start calendar missing",startCalendar.isDisplayed());
        assertTrue("End calendar missing",endCalendar.isDisplayed());
        assertTrue("Type options missing",typeOption.isDisplayed());
        Select typeSelect=new Select(typeOption);
        assertEquals("Wrong type selected","Tous",typeSelect.getFirstSelectedOption().getText());
        List<WebElement> options =typeSelect.getOptions();
        List<String> optionsText=new ArrayList<>();
        for (WebElement option : options) {
            optionsText.add(option.getText());
        }
        assertTrue(optionsText.contains("Ressource normale"));
        assertTrue(optionsText.contains("Ressource en file"));

    }

    public PageWorkers clearSearch(WebDriver driver){
        return searchByDetail("",driver);
    }

    public PageWorkers goNextPage(WebDriver driver){
        Actions a=new Actions(driver);
        a.moveToElement(nextButton).perform();
        nextButton.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }
    public PageWorkers goPreviousPage(WebDriver driver){
        Actions a=new Actions(driver);
        a.moveToElement(previousButton).perform();
        previousButton.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }
    public PageWorkers goLastPage(WebDriver driver){
        Actions a=new Actions(driver);
        a.moveToElement(lastButton).perform();
        lastButton.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }
    public PageWorkers goFirstPage(WebDriver driver){
        Actions a=new Actions(driver);
        a.moveToElement(firstButton).perform();
        firstButton.click();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(title));
        return PageFactory.initElements(driver,PageWorkers.class);
    }
    public int getPageNumber(){
        return Integer.parseInt(pageInput.getAttribute("value"));
    }

    public PageLogin logout(WebDriver driver){
        logout.click();
        return PageFactory.initElements(driver,PageLogin.class);
    }


}
