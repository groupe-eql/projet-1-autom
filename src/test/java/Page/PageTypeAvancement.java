package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageTypeAvancement {
    @FindBy (xpath = "//div[contains(text(),'Types')]")
    private WebElement title;
    @FindBy(xpath = "//div[contains(text(),'Nom')]")
    private WebElement colonneNom;
    @FindBy(xpath = "//div[contains(text(),'Activé')]")
    private WebElement colonneActive;
    @FindBy(xpath = "//div[contains(text(),'Prédéfini')]")
    private WebElement colonnePredefini;
    @FindBy(xpath = "//div[contains(text(),'Opérations')]")
    private WebElement colonneOperations;
    @FindBy(xpath = "//td[contains(text(),'Créer')]")
    private WebElement btnCreer;



    public PageCreerTypeAvancement goToCreerTypeAvancement(WebDriver driver){
    btnCreer.click();
    return PageFactory.initElements(driver,PageCreerTypeAvancement.class);
    }
    public boolean checkTitle(){
       return title.isDisplayed();
    }
    public String colonneNom(){
        return colonneNom.getText();
    }
    public String colonneActive(){
        return colonneActive.getText();
    }
    public String colonnePredifini(){
        return colonnePredefini.getText();
    }
    public String colonneOperations(){
        return colonneOperations.getText();
    }
    public boolean btnCreerDisplayed(){
        return btnCreer.isDisplayed();
    }

    //================================================
    @FindBy (xpath = "//span[contains(text(),'Type ')]")
    private WebElement messageEnregistrement;
    @FindBy(xpath = "//div[contains(@id,'y8')]/span")
    private WebElement nomTypeAvancement;
    @FindBy(xpath = "//div[contains(@id,'u5')]/span/input")
    private WebElement activeDecocher;
    @FindBy(xpath = "//div[contains(@id,'_9')]/span/input]")
    private WebElement predifiniDecocher;
    public boolean messageDisplayed(){
       return messageEnregistrement.isDisplayed();
    }
    public String nomTypeAvancement(){
       return nomTypeAvancement.getText();
    }
    public boolean activeDecrocher(){
       return activeDecocher.isSelected();
    }
    public boolean predifiniDecrocher(){
        return predifiniDecocher.isSelected();
    }
     @FindBy (xpath = "//span[contains(text(),'Test2')]")
    private WebElement nomTest2Displayed;
    @FindBy (xpath = "//span[contains(text(),'Test1')]")
    private WebElement nomTest1Displayed;
    public boolean nomTest1Displayed(){
     return   nomTest1Displayed.isDisplayed();
    }
    public boolean nomTest2Displayed(){
        return   nomTest2Displayed.isDisplayed();
    }
    @FindBy(xpath = "//tr[7]/td[4]/div/table/tbody/tr/td/table/tbody/tr/td[3]/span/table/tbody/tr[2]/td[2]/img")
    private WebElement delete1;
    @FindBy(xpath = "//table[2]/tbody/tr/td/table/tbody/tr/td[1]/span/table/tbody/tr[2]/td[2]")
    private WebElement ok;
    @FindBy(xpath = "//tr[6]/td[4]/div/table/tbody/tr/td/table/tbody/tr/td[3]/span/table/tbody/tr[2]/td[2]/img")
    private WebElement delete2;

    @FindBy(xpath = "/html/body/div[3]/div[3]/div/div/div/table[2]/tbody/tr/td/table/tbody/tr/td[1]/span/table/tbody/tr[2]/td[2]")
    private WebElement ok1;
    public void delete() throws InterruptedException {
        delete1.click();
        ok.click();
        Thread.sleep(1000);
        delete2.click();
        ok1.click();
    }


}
