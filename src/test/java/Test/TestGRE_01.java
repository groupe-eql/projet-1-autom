package Test;

import Page.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;


import static org.junit.Assert.*;

public class TestGRE_01 {
    WebDriver driver;
    String url="http://localhost:8180/libreplan";
    String user="admin";
    String password="admin";
    String firstName="Jean";
    String lastName="DU";
    String id="jdu";
    String newPassword="$jdumdp1";
    String email="jdu@test.fr";
    PageHome pageHome;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws IOException {
        driver.get(url);
        driver.manage().window().maximize();
        //log in
        PageLogin pageLogin= PageFactory.initElements(driver, PageLogin.class);
        pageHome= pageLogin.login(driver,user,password);
        PageWorkers pageWorkers=pageHome.goToWorkers(driver);
        pageWorkers.checkOnPage(driver);
        //create workers id there isn't 15 workers and check worker jdu doesn't exist
        if(pageWorkers.getNbWorkers()==1){
            pageWorkers.createWorkers("src/main/resources/jdd/workers.csv",driver);
        }
        pageWorkers.checkOnPage(driver);
        assertTrue("Not enough workers",pageWorkers.getNbWorkers()>=15);
        assertFalse("Worker jdu already exists",pageWorkers.assertRowExists(lastName,firstName,id,driver));
        //create a worker and his linked user
        PageCreateWorker pageCreateWorker= pageWorkers.createWorker(driver);
        pageCreateWorker.checkOnPersonalData(driver);
        pageWorkers=pageCreateWorker.addWorkerAndUser(firstName,lastName,id,id,newPassword,email,driver);
        pageWorkers.checkOnPage(driver);
        assertTrue(pageWorkers.assertRowExists(lastName,firstName,id,driver));
        //filter with first name
        pageWorkers=pageWorkers.searchByDetail(firstName,driver);
        pageWorkers.checkOnPage(driver);
        assertTrue(pageWorkers.assertRowExists(lastName,firstName,id,driver));
        //click more options button
        pageWorkers.moreOptions(driver);
        pageWorkers=pageWorkers.clearSearch(driver);
        pageWorkers.checkOnPage(driver);
        //go to the next page
        pageWorkers=pageWorkers.goNextPage(driver);
        pageWorkers.checkOnPage(driver);
        assertEquals("Wrong page",2,pageWorkers.getPageNumber());
        //go to the previous page
        pageWorkers=pageWorkers.goPreviousPage(driver);
        pageWorkers.checkOnPage(driver);
        assertEquals("Wrong page",1,pageWorkers.getPageNumber());
        //go to the last page
        pageWorkers=pageWorkers.goLastPage(driver);
        pageWorkers.checkOnPage(driver);
        assertEquals("Wrong page",2,pageWorkers.getPageNumber());
        //go to the first page
        pageWorkers=pageWorkers.goFirstPage(driver);
        pageWorkers.checkOnPage(driver);
        assertEquals("Wrong page",1,pageWorkers.getPageNumber());
        pageLogin=pageWorkers.logout(driver);
        pageHome=pageLogin.login(driver,id,newPassword);
        pageHome.checkOnWorker(driver);
    }

    @After
    public void tearDown(){
        PageLogin pageLogin= pageHome.logout(driver);
        pageHome=pageLogin.login(driver,user,password);
        PageWorkers pageWorkers=pageHome.goToWorkers(driver);
        pageWorkers.checkOnPage(driver);
        pageWorkers=pageWorkers.deleteRow(id,driver);
        pageWorkers.checkOnPage(driver);
        for(int i=2;i<17;++i){
            pageWorkers=pageWorkers.deleteRowWithoutUser(""+i,driver);
            pageWorkers.checkOnPage(driver);
        }
    }





}
