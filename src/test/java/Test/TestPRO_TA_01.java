package Test;

import Page.PageCreationNouveauProjet;
import Page.PageHome;
import Page.PageLogin;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TestPRO_TA_01 {
    String url="http://localhost:8180/libreplan";
    String user="admin";
    String password="admin";
    WebDriver driver;
    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void testCreationProjet() throws InterruptedException {
        PageLogin pageLogin = PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome = pageLogin.login(driver, user, password);
        // Vérifie si le calendrier est affiché sur la page d'accueil
        Assert.assertTrue(pageHome.calendrierDisplayed());
        PageCreationNouveauProjet creationNouveauProjet = pageHome.goToCreationProjet(driver);
        // Vérifie si le champ de nom de projet est vide
        Assert.assertEquals("", creationNouveauProjet.checkNameProject());
        // Vérifie si la liste de loupe (probablement une barre de recherche) est vide
        Assert.assertEquals("", creationNouveauProjet.checkListLoupe());
        // Vérifie la classe CSS du champ (grisé) sur la nouvelle page de création de projet
        Assert.assertEquals("z-textbox z-textbox-disd z-textbox-text-disd", creationNouveauProjet.checkChampsGrise());
        // Vérifie si la case à cocher est sélectionnée
        Assert.assertTrue(creationNouveauProjet.checkBoxSelected());
        LocalDate aujourdhui = LocalDate.now();
        // Définissez le format du motif pour "2 janv. 2024"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.FRENCH);
        // Formatez la date selon le motif
        String dateFormatee = aujourdhui.format(formatter);
        // Vérifie si la date formatée correspond à celle affichée sur la page de création de projet
        Assert.assertEquals(dateFormatee,creationNouveauProjet.checkDate());
        // Vérifie si le champ d'échéance est vide sur la page de création de projet
        Assert.assertEquals("",creationNouveauProjet.checkEcheance());
        // Vérifie si le calendrier par défaut est affiché sur la page de création de projet
        Assert.assertEquals("Default",creationNouveauProjet.checkCalendrier());
        // Vérifie si la liste de loupe client est vide sur la page de création de projet
        Assert.assertEquals("",creationNouveauProjet.checkListLoupeClient());
        // Ajouter 5 jours
        LocalDate dateDansCinqJours = aujourdhui.plusDays(5);
        // Formater la date en "2 janv. 2024"
        String dateFormatee5 = dateDansCinqJours.format(formatter);
        // Ajouter 15 jours
        LocalDate dateDansQuinzeJours = aujourdhui.plusDays(15);
        // Formater la date en "2 janv. 2024"
        String dateFormatee15 = dateDansQuinzeJours.format(formatter);
        // Vérifie si la liste de loupe client est vide sur la page de création de projet
        Assert.assertEquals("", creationNouveauProjet.checkListLoupeClient());
        // Vérifie si le bouton "Accepter" est présent et cliquable sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.btnAccepter());
        // Vérifie si le bouton "Annuler" est présent et cliquable sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.btnAnnuler());

        creationNouveauProjet.remplirChamps("PROJET_TEST1","PRJTEST001",dateFormatee5,dateFormatee15);
        // Vérifie si le détail du projet est présent et visible sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.checkDetailProject());
        // Vérifie si le WBS (Work Breakdown Structure) est présent et visible sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.checkWBS());
        // Vérifie la présence et la visibilité du menu vertical sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.checkMenuVertical());
        // Vérifie la présence et la visibilité du menu horizontal sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.checkMenuHorizontal());
        // Vérifie la présence et la cliquabilité du bouton "Enregistrement" sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.checkBtnEnregistrement(driver));
        // Vérifie la présence et la cliquabilité du bouton "Annuler l'Édition" sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.checkBtnAnuulerEdition(driver));
        // Vérifie le message d'avertissement avant l'annulation des modifications sur la page de création de projet
        Assert.assertEquals("Les modifications non enregistrées seront perdues. Êtes-vous sûr ?",creationNouveauProjet.annulationEditionProjet1());
        // Vérifie si le bouton "Annuler" fonctionne correctement sur la page de création de projet
        Assert.assertTrue(creationNouveauProjet.clickAnnuler());
        // Assert.assertTrue(creationNouveauProjet.clickOK());
        creationNouveauProjet.checkProjects(driver);
        /*  Ces assertions vérifient divers détails du projet, tels que le nom, le code,
        la date, l'échéance, le budget, le client, les heures, l'état, et les libellés des boutons. */
        Assert.assertEquals("PROJET_TEST1",creationNouveauProjet.checkNomProjet());
        Assert.assertEquals("PRJTEST001",creationNouveauProjet.checkCodeProjet());
        Assert.assertEquals(dateFormatee5,creationNouveauProjet.checDateProjet());
        Assert.assertEquals(dateFormatee15,creationNouveauProjet.checkEcheanceProjet());
        Assert.assertEquals("0 €",creationNouveauProjet.checkBudgetProjet());
        Assert.assertEquals("",creationNouveauProjet.checkClientProjet());
        Assert.assertEquals("0",creationNouveauProjet.checkHeuresProjet());
        Assert.assertEquals("PRE-VENTES",creationNouveauProjet.checkEtatProjet());
        Assert.assertEquals("Modifier",creationNouveauProjet.checkModifierProjet());
        Assert.assertEquals("Supprimer",creationNouveauProjet.checkSupprimerProjet());
        Assert.assertEquals("Voir la prévision",creationNouveauProjet.checkPrevisionProjet());
        Assert.assertEquals("Créer un modèle",creationNouveauProjet.checkCreerProjet());

    }
    @After
    public void teardown(){
        PageCreationNouveauProjet creationNouveauProjet = PageFactory.initElements(driver, PageCreationNouveauProjet.class);
        creationNouveauProjet.delete(driver);
        driver.quit();
    }
}
