package Test;

import Page.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class TestFQU_01 {

    String url="http://localhost:8180/libreplan";
    String user="admin";
    String password="admin";
    WebDriver driver;
    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void testGestionFormulaire() throws InterruptedException {
        PageLogin pageLogin = PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome = pageLogin.login(driver, user, password);
        // Vérifie si le calendrier est affiché sur la page d'accueil
        Assert.assertTrue(pageHome.calendrierDisplayed());

        PageFormulaireQualite formulaireQualite = pageHome.goToFormulaireQualite(driver);
        // Vérification si le titre de la page des formulaires de qualité est affiché
        Assert.assertTrue(formulaireQualite.titleDisplayed(driver));
        // Vérification si le tableau des formulaires de qualité est affiché
        Assert.assertTrue(formulaireQualite.tableauFormulaireDisplayed());
        // Vérification si le champ de filtre est présent sur la page des formulaires
        Assert.assertTrue(formulaireQualite.champFiltre());
        // Vérification si le bouton de création de formulaire est affiché
        Assert.assertTrue(formulaireQualite.btnCreerDisplayed());

        PageCreerFormulaire creerFormulaire = formulaireQualite.clickBtnCreer(driver);
        // Vérification si le champ de nom du formulaire est vide
        Assert.assertEquals("",creerFormulaire.checkChampNom());
        // Vérification si le champ de description du formulaire est vide
        Assert.assertEquals("",creerFormulaire.checkChampDescription());
       // Assert.assertEquals("par pourcentage \npar élément",creerFormulaire.checkChampTypeFormulaire());
        // Vérification si le champ d'avancement du formulaire n'est pas coché
        Assert.assertFalse(creerFormulaire.checkChampAvancement());
        // Vérification si les colonne du tableau sont présente sur la page
        Assert.assertTrue(creerFormulaire.checkColonneTableau());
        // Vérification si le bouton "Sauver et Continuer" est présent
        Assert.assertTrue(creerFormulaire.checkBtnSauverContinuer());
        // Vérification si le bouton "Enregistrer" est présent
        Assert.assertTrue(creerFormulaire.checkBtnEnregistrer());
        // Vérification si le bouton "Annuler" est présent
        Assert.assertTrue(creerFormulaire.checkBtnAnnuler());

        creerFormulaire.renseignerFormulaire("Formulaire Test 1","Formulaire Test 1");
        // Vérification si le champ de nom du nouvel élément est vide
        Assert.assertEquals("",creerFormulaire.checkNomNouvelElement());
        // Vérification si la position du nouvel élément est "1"
        Assert.assertEquals("1",creerFormulaire.checkPositionNouvelElement());
        // Vérification si le champ de pourcentage du nouvel élément est vide
        Assert.assertEquals("",creerFormulaire.checkPourcentageNouvelElement());
        // Vérification si les options du nouvel élément sont présentes
        Assert.assertTrue(creerFormulaire.checkOptionsNouvelElement());

        creerFormulaire.creerFormulaireQualite("Formulaire - Element 1","20");
        // Vérification si la position du premier nouvel élément est "2"
        Assert.assertEquals("2",creerFormulaire.checkPositionUnNouvelElement());
        // Vérification si la position du deuxième nouvel élément est "1"
        Assert.assertEquals("1",creerFormulaire.checkPositionDeuxNouvelElement());

        creerFormulaire.creerFormulaireQualite1("Formulaire - Element 2","40");
        // Vérification si le nom du premier élément est "Formulaire - Element 11"
        Assert.assertEquals("Formulaire - Element 11",creerFormulaire.elementPositionUn());
        // Vérification si le nom du deuxième élément est "Formulaire - Element 22"
        Assert.assertEquals("Formulaire - Element 22",creerFormulaire.elementPositionDeux());

        creerFormulaire.clickContinuerSauver();
        // Vérification du message après l'enregistrement du formulaire de qualité
        Assert.assertEquals("Formulaire qualité \"Formulaire Test 1\" enregistré",creerFormulaire.checkMessageFormulairQualite());
        //  Vérification du titre après l'enregistrement du formulaire de qualité
        Assert.assertEquals("Modifier Formulaire qualité: Formulaire Test 1",creerFormulaire.checkTitleFormulairQualite());

        PageFormulaireQualite formulaireQualite1 = creerFormulaire.clickBtnAnnuler(driver);
        // Vérification si le titre de la page des formulaires de qualité est affiché
        Assert.assertTrue(formulaireQualite1.titleDisplayed(driver));

        PageModifierFormulaireQualite modifierFormulaireQualite =formulaireQualite1.clickFormulaireTestUn(driver);
        // Vérification du titre de la page de modification du formulaire
        Assert.assertEquals("Modifier Formulaire qualité: Formulaire Test 1",modifierFormulaireQualite.checkTitleModifierFormulaire(driver));
        // Sélectionne l'option "Par Élément"
        modifierFormulaireQualite.selectParElement();
        // Vérification que la colonne de pourcentage n'est pas affichée
        Assert.assertFalse(modifierFormulaireQualite.checkColonnePourcentage(driver));
        // Clique sur le bouton "Enregistrer" pour enregistrer les modifications du formulaire
        PageFormulaireQualite formulaireQualite2 = modifierFormulaireQualite.clickBtnEnregistrer(driver);
        // Vérification du message après l'enregistrement du formulaire de qualité
       Assert.assertEquals("Formulaire qualité \"Formulaire Test 1\" enregistré",formulaireQualite2.checkmessegeEnregistrer());

    }
    @After
    public void teardown(){
        PageFormulaireQualite formulaireQualite = PageFactory.initElements(driver,PageFormulaireQualite.class);
        formulaireQualite.delete(driver);
        driver.quit();
    }
}
