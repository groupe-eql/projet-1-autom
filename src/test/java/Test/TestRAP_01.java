package Test;

import Page.PageHeureEstimeesPlanifieesParTache;
import Page.PageHome;
import Page.PageLogin;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class TestRAP_01 {
        WebDriver driver;
        @Test
        public void setup() {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
            driver = new ChromeDriver();
            driver.get("http://localhost:8180/libreplan");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
            // Utilise l'objet PageLogin pour se connecter avec les identifiants fournis
            PageLogin pageLogin = PageFactory.initElements(driver, PageLogin.class);
            PageHome pageHome=pageLogin.login(driver,"admin", "admin");
            // Vérifie que le calendrier est affiché sur la page d'accueil
            Assert.assertTrue(pageHome.calendrierDisplayed());
            // Vérifie que le titre de la page est "Heures estimées/planifiées par tâche"
            PageHeureEstimeesPlanifieesParTache heureEstime = pageHome.accesItem(driver);
            // Vérifie la présence de sous-titres spécifiques sur la page
            Assert.assertEquals(heureEstime.checkTitle(),"Heures estimées/planifiées par tâche");
            Assert.assertTrue(heureEstime.checkSubtitle("Date",driver));
            Assert.assertTrue(heureEstime.checkSubtitle("Filtrer par projet",driver));
            Assert.assertTrue(heureEstime.checkSubtitle("Filtrer par libellé",driver));
            Assert.assertTrue(heureEstime.checkSubtitle("Filtrer par critère",driver));
            Assert.assertTrue(heureEstime.checkSubtitle("Format",driver));
            // Vérifie la présence du bouton "Montrer" sur la page
            Assert.assertTrue(heureEstime.checkButtonMontrer());


        }
    }


