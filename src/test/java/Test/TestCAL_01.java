package Test;

import Page.PageCalendrier;
import Page.PageCriteria;
import Page.PageHome;
import Page.PageLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class TestCAL_01 {

    WebDriver driver;
    String url="http://localhost:8080/libreplan";
    String user="admin";
    String password="admin";

    @Before
    public void setUp(){
        // Configuration du pilote Firefox
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Test
    public void test(){
        // Accéder à l'URL
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Initialisation des pages avec PageFactory
        PageLogin pageLogin = PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome = pageLogin.login(driver, user, password);
        PageCalendrier pCal = pageHome.HoverAndSelectListItem(driver);

        // Vérifier la page Liste de Calendriers
        pCal.CheckOnPageListeCalendriers();


        // Ajouter un calendrier avec le nom "test1"
        pCal.AddCalendar(driver);

        // Ajouter un dérivé du calendrier test1
        pCal.AddDeriveCal(driver);

        // Ajouter une copie du calendrier
         pCal.copieCal(driver);

    }
    @After
    public void tearDown(){
        driver.quit();
    }
}
