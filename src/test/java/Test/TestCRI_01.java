package Test;

import Page.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.*;


public class TestCRI_01 {
    WebDriver driver;
    String url="http://localhost:8180/libreplan";
    String user="admin";
    String password="admin";
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test(){
        driver.get(url);
        driver.manage().window().maximize();
        //log in
        PageLogin pageLogin=PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome= pageLogin.login(driver,user,password);
        //go on Criteria page
        PageCriteria pageCriteria= pageHome.goToCriteria(driver);
        pageCriteria.checkOnPage(driver);
        int initRows=pageCriteria.getNbCriteria();
        //go on create criteria page and cancel
        PageCreateCriteria pageCreate=pageCriteria.createNewCriteria(driver);
        pageCreate.checkOnPage(driver);
        pageCriteria=pageCreate.fillAndCancel("Critère - Test bouton [Annuler]","PARTICIPANT",driver);
        pageCriteria.checkOnPage(driver);
        //check the number of rows didn't change
        assertEquals("The number of rows is wrong", initRows,pageCriteria.getNbCriteria());
        //create a new criteria and check it has been added to the list
        pageCreate=pageCriteria.createNewCriteria(driver);
        pageCreate.checkOnPage(driver);
        pageCriteria=pageCreate.fillAndSave("Critère - Test bouton [Enregistrer]","PARTICIPANT",driver);
        pageCriteria.checkOnPage(driver);
        assertEquals("The number of rows is wrong", initRows+1,pageCriteria.getNbCriteria());
        //create a new criteria and continue
        pageCreate=pageCriteria.createNewCriteria(driver);
        pageCreate.checkOnPage(driver);
        PageEditCriteria pageEdit=pageCreate.fillSaveContinue("Critère - Test bouton [Sauver et continuer]","PARTICIPANT",driver);
        pageEdit.checkOnPage("Critère - Test bouton [Sauver et continuer]",driver);
        pageCriteria=pageEdit.cancel(driver);
        pageCriteria.checkOnPage(driver);
        assertEquals("The number of rows is wrong", initRows+2,pageCriteria.getNbCriteria());
        //edit a criteria and cancel
        pageEdit=pageCriteria.editCriteria("Critère - Test bouton [Sauver et continuer]",driver);
        pageEdit.checkOnPage("Critère - Test bouton [Sauver et continuer]",driver);
        pageCriteria=pageEdit.fillAndCancel("Critère - Test bouton [Sauver et continuer]2","PARTICIPANT",driver);
        pageCriteria.checkOnPage(driver);
        assertTrue(pageCriteria.assertRowExists("Critère - Test bouton [Sauver et continuer]",driver));
        assertFalse(pageCriteria.assertRowExists("Critère - Test bouton [Sauver et continuer]2",driver));
        //edit a criteria and save
        pageEdit=pageCriteria.editCriteriaByName("Critère - Test bouton [Sauver et continuer]",driver);
        pageEdit.checkOnPage("Critère - Test bouton [Sauver et continuer]",driver);
        pageEdit.fillSaveContinue("Critère - Test bouton [Sauver et continuer]2","PARTICIPANT",driver);
        pageEdit.checkOnPage("Critère - Test bouton [Sauver et continuer]2",driver);
        pageEdit.checkSaved("Critère - Test bouton [Sauver et continuer]2",driver);
        pageCriteria=pageEdit.cancel(driver);
        pageCriteria.checkOnPage(driver);
        assertFalse(pageCriteria.assertRowExists("Critère - Test bouton [Sauver et continuer]",driver));
        assertTrue(pageCriteria.assertRowExists("Critère - Test bouton [Sauver et continuer]2",driver));
        //delete a criteria and cancel
        pageCriteria.deleteAndCancel("Critère - Test bouton [Sauver et continuer]2",driver);
        assertTrue(pageCriteria.assertRowExists("Critère - Test bouton [Sauver et continuer]2",driver));
        //delete a criteria and save
        pageCriteria.deleteAndSave("Critère - Test bouton [Sauver et continuer]2",driver);
        assertFalse(pageCriteria.assertRowExists("Critère - Test bouton [Sauver et continuer]2",driver));
    }
    @After
    public void tearDown(){
        PageCriteria pageCriteria=PageFactory.initElements(driver,PageCriteria.class);
        pageCriteria.deleteAndSave("Critère - Test bouton [Enregistrer]",driver);
        driver.quit();
    }
}
