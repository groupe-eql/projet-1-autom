package Test;

import Page.PageCreerTypeAvancement;
import Page.PageHome;
import Page.PageLogin;
import Page.PageTypeAvancement;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class TestAVA_01 {
    String url="http://localhost:8180/libreplan";
    String user="admin";
    String password="admin";
    WebDriver driver;
    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void testCreationAvancement() throws InterruptedException {
        PageLogin pageLogin = PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome = pageLogin.login(driver, user, password);
        // Vérifie si le calendrier est affiché sur la page d'accueil
        Assert.assertTrue(pageHome.calendrierDisplayed());
        PageTypeAvancement avancement = pageHome.goToTypeAvancement(driver);
        // Vérifier si le titre de la page est présent
        Assert.assertTrue(avancement.checkTitle());
        // Vérifier si les noms des colonnes sont correct
        Assert.assertEquals("Nom",avancement.colonneNom());
        Assert.assertEquals("Activé",avancement.colonneActive());
        Assert.assertEquals("Prédéfini",avancement.colonnePredifini());
        Assert.assertEquals("Opérations",avancement.colonneOperations());
        // Vérifier si le bouton de création est affiché sur la page type d'avancement
        Assert.assertTrue(avancement.btnCreerDisplayed());

        PageCreerTypeAvancement typeAvancement = avancement.goToCreerTypeAvancement(driver);
        // Vérifier si le bouton de création est affiché sur la page d'avancement
        Assert.assertTrue(typeAvancement.checktitleCreerTypeAvancement());
        // Vérifier si les lignes du tableau sont. présente sur la page de création de Type d'Avancement
        Assert.assertTrue(typeAvancement.checkLigneTableau());
        // Vérifie si le champ "Nom de l'unité" est vide
        Assert.assertEquals("",typeAvancement.champNomUnite());
        // Vérifie si le champ "Actif" est coché
        Assert.assertTrue(typeAvancement.champActif());
//        Assert.assertEquals("100,00",typeAvancement.champValeurMax());
//        Assert.assertEquals("0,1000",typeAvancement.champPrecision());
        // Vérifie si le champ "Type" est égal à "User"
        Assert.assertEquals("User",typeAvancement.champType());
        // Vérifie si le champ "Pourcentage" n'est pas coché
        Assert.assertFalse(typeAvancement.champPourcentage());
        // Vérifie si le bouton "Enregistrer" est présent
        Assert.assertTrue(typeAvancement.checkBtnEnregistrer());
        // Vérifie si le bouton "Sauver et Continuer" est présent
        Assert.assertTrue(typeAvancement.checkBtnSauverContinuer());
        // Vérifie si le bouton "Annuler" est présent
        Assert.assertTrue(typeAvancement.checkBtnAnnuler());
        // Saisir des données dans le tableau sur la page de création de Type d'Avancement
        PageTypeAvancement avancement1 = typeAvancement.SaisirTableau(driver,"Test1", "10,00");

        Assert.assertTrue(avancement1.messageDisplayed());

//        Assert.assertEquals("Test1",avancement1.nomTypeAvancement());
//        Assert.assertFalse(avancement1.activeDecrocher());
//        Assert.assertFalse(avancement1.predifiniDecrocher());

        PageCreerTypeAvancement typeAvancement1 = avancement1.goToCreerTypeAvancement(driver);
        // Vérifie si le titre de la page de création de Type d'Avancement est présent
         Assert.assertTrue(typeAvancement1.checktitleCreerTypeAvancement());

         typeAvancement1.saisirChamps("Test2",driver);
         typeAvancement1.SauverContinuerTest2();
        // Vérifie si le message de succès est affiché
         Assert.assertTrue(typeAvancement1.messageApparu());
        // Vérifie si le titre de la page d'avancement est correct après sauvegarde et continuation
         Assert.assertEquals("Modifier Type d'avancement: Test2",typeAvancement.titleDisplayed());
        // Naviguer vers la page de Type d'Avancement depuis la page de création de Type d'Avancement1
         PageTypeAvancement avancement2 = typeAvancement1.clickBtnAnnuler(driver);
        // Vérifie si les noms de Test1 et Test2 sont affichés sur la page d'avancement2
         Assert.assertTrue(avancement2.nomTest2Displayed());
       //  Assert.assertTrue(avancement2.nomTest1Displayed());

    }
    @After
    public void teardown() throws InterruptedException {
        PageTypeAvancement avancement= PageFactory.initElements(driver,PageTypeAvancement.class);
        avancement.delete();
        driver.quit();
    }

}
