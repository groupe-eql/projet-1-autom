package Test;

import Page.PageCalendrier;
import Page.PageCriteria;
import Page.PageHome;
import Page.PageLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;

public class TestCAL_02 {
    WebDriver driver;
    String url="http://localhost:8080/libreplan";
    String user="admin";
    String password="admin";

    @Before
    public void setUp(){
        // Configuration du pilote Firefox
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Test
    public void test(){
        // Accéder à l'URL
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Initialisation des pages avec PageFactory
        PageLogin pageLogin = PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome = pageLogin.login(driver, user, password);
        PageCalendrier pCal = pageHome.HoverAndSelectListItem(driver);

        // Vérifier la page Liste de Calendriers
        pCal.CheckOnPageListeCalendriers();

        // Ajouter un calendrier
        pCal.AddCalendar(driver);

        //création d'une exception de type "strike" du calendrier
        pCal.ajouterException(driver);

        //Mettre à jour l'exception du calendrier de type "strike" avec 4h05min d'effort normal et 2h30 d'effort supplémentaire
        pCal.MettreaJour_chargedeTravail_Exception(driver,"4","5","2","30");

        // Ajouter un dérivé du calendrier test1
        pCal.AddDeriveCal(driver);

        //Verifier l'héritage de l'exception de dérivé du calendrier "test1"
        pCal.verife_derive_exception(driver);

        //Verifier la copie du calendrier
        pCal.verifie_copie_exception(driver);
    }
    @After
    public void tearDown(){
        driver.quit();
    }
}
