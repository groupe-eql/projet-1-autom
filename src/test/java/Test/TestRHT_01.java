package Test;

import Page.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import java.util.Random;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.junit.Assert.*;

public class TestRHT_01 {
    WebDriver driver;
    String url="http://localhost:8180/libreplan";
    String user="admin";
    String password="admin";
    long dateDelta=3;
    String ressourceID="1";
    String hours="8";
    String template="TestTemplate";

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test(){
        driver.get(url);
        driver.manage().window().maximize();
        //log in
        PageLogin pageLogin= PageFactory.initElements(driver, PageLogin.class);
        PageHome pageHome= pageLogin.login(driver,user,password);
        //go to tn time sheets page
        PageTimeSheets pageTimeSheets=pageHome.goToTimeSheets(driver);
        pageTimeSheets.checkOnPage(driver);
        PageCreateTimeSheet pageCreate;
        //create timesheets
        for (int i=0;i<3;++i){
            Random rand = new Random();
            int delta=rand.nextInt(50);
            Integer hour=rand.nextInt(20);
            if (hour==0){
                hour++;
            }
            pageCreate= pageTimeSheets.createTimeSheet(driver);
            pageCreate.checkOnPage(driver);
            pageTimeSheets=pageCreate.addLineNotDone(delta,ressourceID,hour.toString(),driver);
            pageTimeSheets.checkOnPage(driver);
        }
        PageSheetTemplate pageSheetTemplate= pageTimeSheets.goToTemplate(driver);
        pageSheetTemplate=pageSheetTemplate.createTemplate(template,driver);
        pageTimeSheets=pageSheetTemplate.createSheetFromTemplate(5,ressourceID,"12",template,driver);
        pageTimeSheets.checkOnPage(driver);
        pageCreate=pageTimeSheets.createTimeSheet(driver);
        pageCreate.checkOnPage(driver);
        pageTimeSheets=pageCreate.addLine(dateDelta,ressourceID,hours,driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertRowExists(dateDelta,hours,driver));
        //assert start dates are in descending order
        assertTrue(pageTimeSheets.assertDateDescending(pageTimeSheets.getStartDates(driver)));
        pageTimeSheets=pageTimeSheets.orderByEndDate(driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertDateAscending(pageTimeSheets.getEndDates(driver)));
        pageTimeSheets=pageTimeSheets.orderByEndDate(driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertDateDescending(pageTimeSheets.getEndDates(driver)));
        pageTimeSheets=pageTimeSheets.orderByTotalWork(driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertNumbersAscending(pageTimeSheets.getTotalWork(driver)));
        pageTimeSheets=pageTimeSheets.orderByTotalWork(driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertNumbersDescending(pageTimeSheets.getTotalWork(driver)));
        pageTimeSheets=pageTimeSheets.orderByTemplate(driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertStringAscending(pageTimeSheets.getTemplates(driver)));
        pageTimeSheets=pageTimeSheets.orderByTemplate(driver);
        pageTimeSheets.checkOnPage(driver);
        assertTrue(pageTimeSheets.assertStringDescending(pageTimeSheets.getTemplates(driver)));
        pageTimeSheets.resizeStartCell(50,driver);
        pageTimeSheets.checkOnPage(driver);
        //pageTimeSheets.resizeStartCell(-100,driver);
    }
}
